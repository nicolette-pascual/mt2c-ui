import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt2c_ui/screens/instructions_overview.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/utils/helper_functions/navigation_helper.dart';
import 'generated/i18n.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: ColorHelper.backgroundColor));

  _registerGoogleFontLicense();

  runApp(MyApp());
}

_registerGoogleFontLicense() {
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: GenericAssets.getAppTheme(context),
      localizationsDelegates: [S.delegate],
      supportedLocales: S.delegate.supportedLocales,
      onGenerateTitle: (context) => S.of(context).strAppTitle,
      onGenerateRoute: NavigationHelper.getRoutes,
      onUnknownRoute: (RouteSettings setting) => CupertinoPageRoute(
          builder: (context) => InstructionsOverviewScreen()),
    );
  }
}
