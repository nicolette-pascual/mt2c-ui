import 'dart:convert';

import 'package:flutter/cupertino.dart';

class Currency {
  String isoCode;
  String currencyName;

  Currency({@required this.isoCode, @required this.currencyName});

  String toJson() {
    Map<String, dynamic> values = {
      'currencyName': currencyName,
      'isoCode': isoCode,
    };
    return json.encode(values);
  }

  Currency.fromJson(Map<String, dynamic> json)
      : currencyName = json['currencyName'],
        isoCode = json['isoCode'];
}
