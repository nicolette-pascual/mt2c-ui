import 'dart:convert';

import 'package:flutter/cupertino.dart';

class Country {
  String isoCode;
  String name;

  Country({@required this.isoCode, @required this.name});

  String toJson() {
    Map<String, dynamic> values = {
      'name': name,
      'isoCode': isoCode,
    };
    return json.encode(values);
  }

  Country.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        isoCode = json['isoCode'];
}
