import 'dart:convert';

import 'package:mt2c_ui/utils/constants/project_constants.dart';

class Transaction {
  String name;
  String status;
  double amount;
  String currency;
  String date;
  TransferType transferType;

  Transaction(
      {this.name,
      this.status,
      this.amount,
      this.currency,
      this.date,
      this.transferType});

  String toJson() {
    Map<String, dynamic> values = {
      'name': name,
      'status': status,
      'amount': amount,
      'currency': currency,
      'date': date,
      'transferType': transferType
    };
    return json.encode(values);
  }

  Transaction.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        status = json['status'],
        amount = json['amount'],
        currency = json['currency'],
        date = json['date'],
        transferType = json['transferType'];
}
