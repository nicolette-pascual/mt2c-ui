import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/page_view_model.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class DashboardScreen extends StatelessWidget {
  final double currentPage;
  final int pageIndex;
  final PageViewModel page;
  final int pageLength;

  const DashboardScreen(
      {Key key, this.currentPage, this.page, this.pageIndex, this.pageLength})
      : super(key: key);

  Widget _buildContentWidget(Widget widget, String text, TextStyle style) {
    return widget ?? Text(text, style: style, textAlign: TextAlign.center);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 70),
          Text(page.title, style: GenericAssets.titleDefaultBlack),
          Padding(
            padding: GenericAssets.paddingBottom30,
            child: DotsIndicator(
              dotsCount: pageLength,
              position: currentPage,
              decorator: DotsDecorator(
                color: ColorHelper.grey, // Inactive color
                activeColor: ColorHelper.primaryColor,
              ),
            ),
          ),
          page.bodyWidget ?? Container(),
        ],
      ),
    );
  }
}
