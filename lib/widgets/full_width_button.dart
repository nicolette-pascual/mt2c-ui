import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class FullWidthButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  final double widthPercentage;
  final EdgeInsets customPadding;

  const FullWidthButton(this.title, this.onPressed,
      {Key key, this.widthPercentage, this.customPadding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;

    return Padding(
      padding: customPadding ?? GenericAssets.paddingBottom15,
      child: SizedBox(
        width: _width * (widthPercentage ?? 0.9),
        height: 50,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          elevation: 2,
          onPressed: onPressed,
          child: Text(
            title,
            style: GenericAssets.buttonText,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
