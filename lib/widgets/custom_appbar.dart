import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:mt2c_ui/generated/i18n.dart';
import 'package:mt2c_ui/screens/menu/dashboard_overview.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/appbar_title.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool isHomePage;
  final Function onLeadingPressed;

  @override
  final Size preferredSize;

  CustomAppBar(
      {Key key, this.title, this.isHomePage = false, this.onLeadingPressed})
      : preferredSize = Size.fromHeight(ProjectConstants.appBarHeight);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  _onBack() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.light,
      title: AppBarTitle(widget.title ?? ''),
      centerTitle: true,
      backgroundColor: ColorHelper.transparent,
      elevation: 0,
      automaticallyImplyLeading: false,
      leading: widget.isHomePage
          ? null
          : Transform.translate(
              offset: Offset(-10, 0),
              child: IconButton(
                icon: const Icon(Icons.keyboard_backspace),
                onPressed: widget.onLeadingPressed ?? _onBack,
              ),
            ),
      actions: <Widget>[
        Padding(
          padding: GenericAssets.paddingRight10,
          child: widget.isHomePage
              ? OpenContainer(
                  closedColor: ColorHelper.transparent,
                  openColor: ColorHelper.transparent,
                  closedElevation: 0.0,
                  openElevation: 0.0,
                  transitionDuration: Duration(milliseconds: 500),
                  closedBuilder: (BuildContext c, VoidCallback action) =>
                      _buildMenuActionButton(),
                  openBuilder: (BuildContext c, VoidCallback action) =>
                      DashboardOverviewScreen(),
                  tappable: true,
                )
              : Container(
                  width: 32.0,
                ),
        )
      ],
    );
  }

  Widget _buildMenuActionButton() {
    return Row(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(0.0),
          width: 32.0, // you can adjust the width as you need
          child: Icon(Icons.menu),
        ),
        Text(S.of(context).strAppbarMenu, style: GenericAssets.smallTextOrange)
      ],
    );
  }
}
