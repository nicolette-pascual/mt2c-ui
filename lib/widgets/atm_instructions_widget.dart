import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/assets.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class AtmInstructionsWidget extends StatelessWidget {
  final bool isSuccessPage;

  const AtmInstructionsWidget({Key key, this.isSuccessPage = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: isSuccessPage
          ? _getRowViewWidgets(size)
          : _getColumnViewWidgets(size),
    );
  }

  List<Widget> _getRowViewWidgets(size) {
    return <Widget>[
      Padding(
        padding: GenericAssets.padding10,
        child: Text('How ATM cash deposit works',
            style: GenericAssets.defaultTextBlueXBold),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _getRowViewInstructionWidget(Assets.ATM_ICON, "Visit ATM",
                "Bring the required amount in cash.\nType the 9-digit transfer number.\nDeposit the cash."),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _getRowViewInstructionWidget(
                Assets.CASH_ICON,
                "Transfer Done",
                "We will send a confirmation via SMS.\nAnd your recepient will get transfer."),
          )
        ],
      )
    ];
  }

  Widget _getRowViewInstructionWidget(image, title, body) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Image.asset(
          image,
          height: 69,
          width: 69,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child:
                  Text(title ?? '', style: GenericAssets.defaultTextGreenBold),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text(body ?? '', style: GenericAssets.smallTitleBlue),
            ),
          ],
        )
      ],
    );
  }

  List<Widget> _getColumnViewWidgets(size) {
    return <Widget>[
      SizedBox(height: size.height * 0.03),
      Padding(
        padding: GenericAssets.padding10,
        child: Text('How sending with ATM works',
            style: GenericAssets.titleDefault),
      ),
      _getInstructionContainer(
          size,
          Assets.ONLINE_ICON,
          "1. Create an order online",
          "Choose where and how much you are planning to send. Fill in recepient details. Receive a 9-digit transfer number."),
      _getInstructionContainer(size, Assets.ATM_ICON, "2. Visit ATM",
          "Bring the required amount in cash.\nType the 9-digit transfer number.\nDeposit the cash."),
      _getInstructionContainer(size, Assets.CASH_ICON, "3. Done",
          "We will send a confirmation via SMS. And your recepient will get your transfer.\nNo waiting in line")
    ];
  }

  Widget _getInstructionContainer(size, image, title, body) {
    return Padding(
      padding: GenericAssets.paddingSymV10,
      child: Container(
        height: size.height * 0.3,
        width: size.width * 0.9,
        color: Colors.white,
        child: LayoutBuilder(builder: (context, constraints) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                image,
                height: constraints.minWidth * 0.25,
                width: constraints.minHeight * 0.4,
              ),
              Text(title, style: GenericAssets.instructionTitle),
              Padding(
                padding: GenericAssets.paddingSymH20,
                child: Text(body,
                    style: GenericAssets.instructionBody,
                    textAlign: TextAlign.center),
              ),
            ],
          );
        }),
      ),
    );
  }
}
