import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class CountdownTimer extends StatefulWidget {
  final EdgeInsets padding;
  final Text text;

  const CountdownTimer({Key key, this.padding, this.text}) : super(key: key);

  @override
  _CountdownTimerState createState() => _CountdownTimerState();
}

class _CountdownTimerState extends State<CountdownTimer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? GenericAssets.paddingNone,
      child: Container(
        child: widget.text,
      ),
    );
  }
}
