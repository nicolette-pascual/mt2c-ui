import 'dart:math';

import 'package:flutter/material.dart';

class PulseAnimationWidget extends StatefulWidget {
  final AnimationController controller;
  final Widget child;

  const PulseAnimationWidget({Key key, this.controller, this.child})
      : super(key: key);
  @override
  _PulseAnimationWidgetState createState() => _PulseAnimationWidgetState();
}

class _PulseAnimationWidgetState extends State<PulseAnimationWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: CustomPaint(
              painter: PulsePainter(widget.controller),
              child: SizedBox(
                width: 150.0,
                height: 150.0,
              )),
        ),
        Positioned(
          top: 35,
          left: 0,
          right: 0,
          child: widget.child,
        ),
      ],
    );
  }
}

class PulsePainter extends CustomPainter {
  final Animation<double> _animation;

  PulsePainter(this._animation) : super(repaint: _animation);

  void circle(Canvas canvas, Rect rect, double value) {
    double opacity = (1.0 - (value / 4.0)).clamp(0.0, 1.0);
    Color color = Color.fromRGBO(255, 136, 2, opacity);

    double size = rect.width / 2;
    double area = size * size;
    double radius = sqrt(area * value / 4);

    final Paint paint = Paint()..color = color;
    canvas.drawCircle(rect.center, radius, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = Rect.fromLTRB(0.0, 0.0, size.width, size.height);

    for (int wave = 3; wave >= 0; wave--) {
      circle(canvas, rect, wave + _animation.value);
    }
  }

  @override
  bool shouldRepaint(PulsePainter oldDelegate) {
    return false;
  }
}
