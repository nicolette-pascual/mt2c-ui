import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mt2c_ui/widgets/designs/animated_wave.dart';

class AppBackground extends StatelessWidget {
  final Widget child;
  final Color customColor;

  const AppBackground({Key key, this.child, this.customColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _getWaveBackground(context, customColor),
        Center(
          child: child,
        ),
      ],
    );
  }

  Widget _getWaveBackground(BuildContext context, Color customColor) {
    var height = MediaQuery.of(context).size.height;
    return RotatedBox(
      quarterTurns: 2, // places the background on top of the screen
      child: Stack(
        children: <Widget>[
          // the following waves takes up approx. 3/4 of the screen
          onBottom(AnimatedWave(
            // 880 - fixed point for wave that takes up 3/4 of the screen,
            // originally 180  - 1/4 of the screen
            height: height * 0.7,
            speed: 1.0,
            customColor: customColor,
          )),
          onBottom(AnimatedWave(
            // 820 - fixed point for wave that takes up 3/4 of the screen,
            // originally 120  - 1/4 of the screen
            height: height * 0.65,
            speed: 0.9,
            offset: pi,
            customColor: customColor,
          )),
          onBottom(AnimatedWave(
            // 920 - fixed point for wave that takes up 3/4 of the screen,
            // originally 220 - 1/4 of the screen
            height: height * 0.8,
            speed: 1.0,
            offset: pi / 2,
            customColor: customColor,
          )),
        ],
      ),
    );
  }

  onBottom(Widget child) => Positioned.fill(
          child: Align(
        alignment: Alignment.bottomCenter,
        child: child,
      ));
}
