import 'package:flutter/material.dart';

class ShaderMaskWidget extends StatefulWidget {
  final Widget child;
  final List<Color> colors;

  const ShaderMaskWidget({Key key, this.child, this.colors})
      : assert(child != null, colors != null),
        super(key: key);

  @override
  _ShaderMaskWidgetState createState() => _ShaderMaskWidgetState();
}

class _ShaderMaskWidgetState extends State<ShaderMaskWidget> {
  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcATop,
      shaderCallback: (rect) {
        return LinearGradient(colors: widget.colors)
            .createShader(Offset.zero & rect.size);
      },
      child: widget.child,
    );
  }
}
