import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:simple_animations/simple_animations.dart';

class AnimatedWave extends StatelessWidget {
  final double height; // sets the area in which the wave acts
  final double speed; // controls the animation duration of a wave pass
  final double offset; // a shift in the x-axis to give a wave different 'start
  final Color customColor;

  const AnimatedWave(
      {Key key, this.height, this.speed, this.offset = 0.0, this.customColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        height: height,
        width: constraints.biggest.width,
        child: ControlledAnimation(
            playback: Playback.LOOP,
            duration: Duration(milliseconds: (5000 / speed).round()),
            tween: Tween(begin: 0.0, end: 2 * pi),
            builder: (context, value) {
              return CustomPaint(
                foregroundPainter: CurvePainter((value + offset), customColor),
              );
            }),
      );
    });
  }
}

class CurvePainter extends CustomPainter {
  final double value;
  final Color customColor;

  CurvePainter(this.value, this.customColor);

  @override
  void paint(Canvas canvas, Size size) {
    final white = Paint()
      ..color = customColor ?? ColorHelper.primaryColor.withAlpha(20);
    final path = Path();

    final y1 = sin(value);
    final y2 = sin(value + pi / 2);
    final y3 = sin(value + pi);

    // original points - takes approx. 1/4 of the screen
    /*final startPointY = size.height * (0.5 + 0.4 * y1);
    final controlPointY = size.height * (0.5 + 0.4 * y2);
    final endPointY = size.height * (0.5 + 0.4 * y3);*/

    // decrease points to accommodate bigger wave area
    final startPointY = size.height * (0.2 + 0.1 * y1);
    final controlPointY = size.height * (0.2 + 0.1 * y2);
    final endPointY = size.height * (0.2 + 0.1 * y3);

    path.moveTo(size.width * 0, startPointY);
    path.quadraticBezierTo(
        size.width * 0.5, controlPointY, size.width, endPointY);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    canvas.drawPath(path, white);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
