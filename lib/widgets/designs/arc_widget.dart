import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';

class ArcWidget extends StatelessWidget {
  final double arcPointA, arcPointB, arcPointC, customHeight, customWidth;
  final Widget child;
  final bool useAsBackground;

  const ArcWidget(
      {Key key,
      this.arcPointA,
      this.arcPointB,
      this.arcPointC,
      this.customHeight,
      this.customWidth,
      this.child,
      this.useAsBackground = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return useAsBackground
            ? Stack(
                children: <Widget>[
                  Container(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    child: CustomPaint(
                      painter: CurvePainter(arcPointA, arcPointB, arcPointC),
                    ),
                  ),
                  Center(
                    child: child,
                  ),
                ],
              )
            : Container(
                // takes the whole screen as canvas if not specified
                height: customHeight ?? constraints.biggest.height,
                width: customWidth ?? constraints.biggest.width,
                child: CustomPaint(
                  painter: CurvePainter(arcPointA, arcPointB, arcPointC),
                ),
              );
      },
    );
  }
}

class CurvePainter extends CustomPainter {
  double arcPointA, arcPointB, arcPointC;

  CurvePainter(this.arcPointA, this.arcPointB, this.arcPointC);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawPath(
        getArcPath(size, arcPointA, arcPointB, arcPointC), customPaint());
  }

  Paint customPaint() {
    Paint paint = Paint();
    paint.color = ColorHelper.backgroundColor;
    paint.isAntiAlias = true;
    paint.strokeWidth = 10;
    return paint;
  }

  Path getArcPath(Size size, arcPointA, arcPointB, arcPointC) {
    double _arcPointA = size.height * arcPointA; // starting point of the arc
    double _arcPointB =
        size.height * arcPointB; // determines the height of the arc's curve
    double _arcPointC = size.height * arcPointC; // ending point of the arc
    return Path()
      ..moveTo(0, _arcPointA)
      ..quadraticBezierTo(size.width / 2, _arcPointB, size.width, _arcPointC)
      ..lineTo(size.width, 0)
      ..lineTo(0, 0);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
