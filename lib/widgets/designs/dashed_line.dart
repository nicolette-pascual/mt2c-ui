import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';

class DashedLine extends StatelessWidget {
  final double height;
  final Color color;
  final double dashWidth;

  const DashedLine(
      {this.height = 1,
      this.color = ColorHelper.grey300,
      this.dashWidth = 10.0});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: height,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
