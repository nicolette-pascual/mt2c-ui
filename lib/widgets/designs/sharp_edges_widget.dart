import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'dart:math' as math;

class SharpEdgesWidget extends StatelessWidget {
  final double customHeight, customWidth;

  const SharpEdgesWidget({Key key, this.customHeight, this.customWidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          // takes the whole screen as canvas if not specified
          height: customHeight ?? constraints.biggest.height,
          width: customWidth ?? constraints.biggest.width,
          child: CustomPaint(
            painter: CurvePainter(),
          ),
        );
      },
    ); //,
    //);
  }
}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final start = Offset(25, size.height * 0.65);
    final end = Offset(size.width * 0.51, size.height * 0.65);

    paintZigZag(
        canvas, customPaint(), start, end, 25, -(size.width * 0.03), size);
    //canvas.drawShadow(getArcPath(size), Colors.grey.withAlpha(50), 4.0, false);
  }

  Paint customPaint() {
    Paint paint = Paint();
    paint.color = ColorHelper.grey300;
    paint.isAntiAlias = true;
    paint.strokeWidth = 1;
    paint.style = PaintingStyle.stroke;
    return paint;
  }

  Paint customPaint2() {
    Paint paint = Paint();
    paint.color = ColorHelper.grey300;
    paint.isAntiAlias = true;
    paint.strokeWidth = 1;
    paint.style = PaintingStyle.fill;
    paint.maskFilter = MaskFilter.blur(BlurStyle.normal, 10);
    return paint;
  }

  Paint customPaint3() {
    Paint paint = Paint();
    paint.color = ColorHelper.white;
    paint.isAntiAlias = true;
    paint.strokeWidth = 1;
    paint.style = PaintingStyle.fill;
    return paint;
  }

  void paintZigZag(Canvas canvas, Paint paint, Offset start, Offset end,
      int zigs, double width, Size size) {
    assert(zigs.isFinite);
    assert(zigs > 0);
    canvas.save();
    end = end - start;
    canvas.rotate(math.atan2(end.dy, end.dx));
    final double length = end.distance;
    final double spacing = length / (zigs * 1.0);
    final Path path = Path()..moveTo(0.0, 0.0);
    for (int index = 0; index < zigs; index += 1) {
      final double x = (index * 2.0 + 1.0) * spacing;
      final double y = width * ((index % 2.0) * 2.0 - 1.0);
      path.lineTo(x, y);
    }
    //path.lineTo(length, 0.0);
    //canvas.drawPath(getArcPath(size), customPaint2());
    //canvas.drawPath(getArcPath(size), customPaint2());
    //canvas.drawShadow(getArcPath(size), Colors.grey.withAlpha(50), 4.0, false);
    //canvas.drawPath(getArcPath2(size), customPaint3());
    canvas.drawLine(Offset(25, 0), start, customPaint());
    canvas.drawLine(Offset(size.width * 0.95, 0.75),
        Offset(size.width * 0.94, size.height * 0.665), customPaint());
    canvas.translate(start.dx, start.dy);
    canvas.drawPath(path, paint);
    canvas.restore();
  }

  Path getArcPath(Size size) {
    /*double _arcPointA = size.height * arcPointA; // starting point of the arc
    double _arcPointB =
        size.height * arcPointB; // determines the height of the arc's curve
    double _arcPointC = size.height * arcPointC; // ending point of the arc
    */ /*return Path()
      ..moveTo(0, 0)
      ..lineTo(0, size.width)
      ..lineTo(size.width, size.height / 2)
      ..lineTo(0, 0);*/

    /*return     Path()..addPolygon([
      /*Offset(0, 0),
      Offset(0, size.height/2),
      Offset(size.width, size.height/2),*/
      Offset(size.width, 0),
      Offset(size.width, size.height * 0.5),
      Offset(0, size.height * 0.5),
      Offset(0, 0),
    ], true);*/

    return Path()
      ..addPolygon([
        /*Offset(0, 0),
      Offset(0, size.height/2),
      Offset(size.width, size.height/2),*/
        /*Offset(25, 0),
      Offset(25, size.height * 0.65),
      Offset(size.width * 0.94, size.height * 0.65),
      Offset(size.width * 0.95, 0.75),
    ], true);*/
        Offset(20, 0),
        Offset(20, size.height * 0.65),
        Offset(size.width * 0.94, size.height * 0.65),
        Offset(size.width * 0.95, 0.75),
      ], true);
  }

  Path getArcPath2(Size size) {
    /*double _arcPointA = size.height * arcPointA; // starting point of the arc
    double _arcPointB =
        size.height * arcPointB; // determines the height of the arc's curve
    double _arcPointC = size.height * arcPointC; // ending point of the arc
    */ /*return Path()
      ..moveTo(0, 0)
      ..lineTo(0, size.width)
      ..lineTo(size.width, size.height / 2)
      ..lineTo(0, 0);*/

    /*return     Path()..addPolygon([
      /*Offset(0, 0),
      Offset(0, size.height/2),
      Offset(size.width, size.height/2),*/
      Offset(size.width, 0),
      Offset(size.width, size.height * 0.5),
      Offset(0, size.height * 0.5),
      Offset(0, 0),
    ], true);*/

    return Path()
      ..addPolygon([
        /*Offset(0, 0),
      Offset(0, size.height/2),
      Offset(size.width, size.height/2),*/
        Offset(25, 0),
        Offset(25, size.height * 0.62),
        Offset(size.width * 0.94, size.height * 0.65),
        Offset(size.width * 0.95, 0.75),
      ], true);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
