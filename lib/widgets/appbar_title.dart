import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class AppBarTitle extends StatelessWidget {
  final String title;

  AppBarTitle(this.title) : assert(title != null);

  @override
  Widget build(BuildContext context) {
    return Text(title, style: GenericAssets.appBarTitle);
  }
}
