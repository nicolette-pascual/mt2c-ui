import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/page_view_model.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class IntroductionScreen extends StatelessWidget {
  final double currentPage;
  final int pageIndex;
  final PageViewModel page;

  const IntroductionScreen(
      {Key key, this.currentPage, this.page, this.pageIndex})
      : super(key: key);

  Widget _buildContentWidget(Widget widget, String text, TextStyle style) {
    return widget ?? Text(text, style: style, textAlign: TextAlign.center);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (page.image != null)
            ? Flexible(
                flex: 2,
                child: Padding(
                    padding: GenericAssets.paddingBottom24,
                    child: AnimatedOpacity(
                        duration: Duration(milliseconds: 300),
                        opacity: (currentPage.round() == pageIndex) ? 1 : 0,
                        child: page.image)),
              )
            : Container(),
        Flexible(
          flex: 1,
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Padding(
              padding: GenericAssets.padding15,
              child: Column(
                children: [
                  Padding(
                    padding: GenericAssets.paddingBottom24,
                    child: _buildContentWidget(page.titleWidget, page.title,
                        GenericAssets.titleDefault),
                  ),
                  Padding(
                    padding: GenericAssets.paddingNone,
                    child: _buildContentWidget(
                      page.bodyWidget,
                      page.body,
                      GenericAssets.defaultText,
                    ),
                  ),
                  (page.footer != null)
                      ? Padding(
                          padding: GenericAssets.paddingSymV24,
                          child: page.footer,
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
