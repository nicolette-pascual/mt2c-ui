import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class ScreenTitleInstructionWidget extends StatelessWidget {
  final String title;
  final String instruction;

  const ScreenTitleInstructionWidget({Key key, this.title, this.instruction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Material(
                    type: MaterialType.transparency,
                    child:
                        Text(title ?? '', style: GenericAssets.titleDefault)),
                Padding(
                  padding: (instruction == null)
                      ? GenericAssets.paddingNone
                      : GenericAssets.paddingTop10,
                  child:
                      Text(instruction ?? '', style: GenericAssets.appBarTitle),
                )
              ],
            )));
  }
}
