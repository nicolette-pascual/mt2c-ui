import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class AmountField extends StatefulWidget {
  final TextEditingController amountController;
  final FocusNode amountFocusNode;
  final Currency currency;

  const AmountField(
      {Key key, this.amountController, this.amountFocusNode, this.currency})
      : assert(amountController != null),
        super(key: key);

  @override
  _AmountFieldState createState() => _AmountFieldState();
}

class _AmountFieldState extends State<AmountField> {
  TextEditingController _amountController;
  FocusNode _amountFocusNode;
  String value;

  @override
  void initState() {
    _amountController = widget.amountController;
    _amountFocusNode = widget.amountFocusNode;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: DynamicTextField(
                controller: _amountController,
                focusNode: _amountFocusNode,
                textStyle: GenericAssets.amountText),
          ),
          Padding(
            padding: GenericAssets.paddingTop10,
            child: Text(widget?.currency?.currencyName ?? 'CZK',
                style: GenericAssets.amountSuffixText),
          ),
        ],
      ),
    );
  }
}

class DynamicTextField extends StatefulWidget {
  final double minWidth;
  final TextEditingController controller;
  final TextStyle textStyle;
  final FocusNode focusNode;

  const DynamicTextField(
      {Key key,
      this.minWidth: 30,
      this.controller,
      this.textStyle,
      this.focusNode})
      : assert(controller != null, textStyle != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() => DynamicTextFieldState();
}

class DynamicTextFieldState extends State<DynamicTextField> {
  var maxWidth;

  // 2.0 is the default from TextField class
  static const _CURSOR_WIDTH = 1.0;

  TextEditingController fieldController;

  // Use this text style for the TextPainter to calculate the width
  // Also used for the TextField to calculate the correct size of the text
  // being displayed
  TextStyle textStyle;

  // Used for adjusting TextField's content padding
  static double contentPaddingL = 0, contentPaddingR = 0;

  initState() {
    fieldController = widget.controller;
    textStyle = widget.textStyle;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // To be used for adjusting the text field's maximum width
    var screenWidth = MediaQuery.of(context).size.width;

    // Use TextPainter to calculate the width of our text
    TextSpan txtSpan = TextSpan(style: textStyle, text: fieldController.text);
    TextPainter txtPainter = TextPainter(
      text: txtSpan,
      textDirection: TextDirection.ltr,
    );
    txtPainter.layout();

    // Enforce a minimum width
    final textWidth = max(widget.minWidth, txtPainter.width + _CURSOR_WIDTH);

    return Container(
      width: maxWidth ?? textWidth,
      child: TextField(
        enableSuggestions: false,
        autocorrect: false,
        cursorWidth: _CURSOR_WIDTH,
        style: textStyle,
        controller: fieldController,
        cursorColor: Colors.orangeAccent,
        onChanged: (text) {
          if (text.substring(1) == '0') {
            text.substring(1);
          }
          // Redraw the widget
          setState(() {
            if (textWidth > (screenWidth * 0.65)) {
              maxWidth = screenWidth * 0.65;
            } else {
              maxWidth = null;
            }
          });
        },
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
          CurrencyInputFormatter()
        ],
        textAlign: TextAlign.center,
        autofocus: true,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: -10, right: -5),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          fillColor: ColorHelper.transparent,
        ),
      ),
    );
  }
}

class CurrencyInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }
    double value = double.parse(newValue.text);
    final formatter = ProjectConstants.amountFormat;
    String newText = formatter.format(value);
    return newValue.copyWith(
        text: newText.replaceAll(',', ' '),
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}
