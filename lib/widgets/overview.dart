import 'dart:math';

import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mt2c_ui/models/page_view_model.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/dashboard_screen.dart';
import 'package:mt2c_ui/widgets/introduction_screen.dart';

class Overview extends StatefulWidget {
  final List<PageViewModel> pages;
  final Function onDone;
  final Function onSkip;
  final bool useDashboardLayout;
  final VoidCallback onFinalSwipeCallback;

  const Overview(
      {Key key,
      @required this.pages,
      @required this.onDone,
      @required this.onSkip,
      this.useDashboardLayout = false,
      this.onFinalSwipeCallback})
      : super(key: key);

  @override
  _OverviewState createState() => _OverviewState();
}

class _OverviewState extends State<Overview> {
  double _currentPage = 0.0;
  PageController _pageController;
  bool _isScrolling = false;
  bool _isLastPage = false;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void next() {
    animateScroll(min(_currentPage.round() + 1, widget.pages.length - 1));
  }

  void prev() {
    animateScroll(min(_currentPage.round() - 1, widget.pages.length - 1));
  }

  bool _onScroll(ScrollNotification notification) {
    final metrics = notification.metrics;
    if (metrics is PageMetrics) {
      setState(() => _currentPage = metrics.page);
    }
    return false;
  }

  Future<void> animateScroll(int page) async {
    setState(() => _isScrolling = true);
    await _pageController.animateToPage(
      page,
      duration: Duration(milliseconds: 350),
      curve: Curves.easeIn,
    );
    if (mounted) {
      setState(() => _isScrolling = false);
    }
  }

  _setLastPage() {
    setState(() {
      _isLastPage = (_currentPage.round() == widget.pages.length - 1);
    });
  }

  _detectSwipe(details) {
    if (details.delta.dx > 0) {
      // left swipe
      prev();
    }
    if (details.delta.dx < 0) {
      // right swipe
      if (_isLastPage) {
        widget.onFinalSwipeCallback();
      } else {
        next();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    _setLastPage();

    return GestureDetector(
      onPanUpdate: _detectSwipe,
      child: Stack(
        children: <Widget>[
          NotificationListener<ScrollNotification>(
            onNotification: _onScroll,
            child: PageView.builder(
              physics: const NeverScrollableScrollPhysics(),
              controller: _pageController,
              itemCount: widget.pages.length,
              onPageChanged: (int page) {
                if (_currentPage != page) {
                  setState(() {
                    _currentPage = double.tryParse(page.toString());
                  });
                }
              },
              itemBuilder: (context, position) {
                return widget.useDashboardLayout
                    ? DashboardScreen(
                        pageIndex: position,
                        currentPage: _currentPage,
                        pageLength: widget.pages.length,
                        page: widget.pages[position],
                      )
                    : IntroductionScreen(
                        pageIndex: position,
                        currentPage: _currentPage,
                        page: widget.pages[position],
                      );
              },
            ),
          ),
          widget.useDashboardLayout
              ? Container()
              : _buildPageViewButtons(_isLastPage)
        ],
      ),
    );
  }

  Widget _buildPageViewButtons(isLastPage) {
    return Positioned(
      bottom: 16.0,
      left: 16.0,
      right: 16.0,
      child: SafeArea(
        child: Center(
          child: Row(
            children: [
              Expanded(
                child: PageViewButton(
                  title: 'Skip',
                  onPressed: widget.onSkip,
                ),
              ),
              Expanded(
                child: Center(
                    child: DotsIndicator(
                  dotsCount: widget.pages.length,
                  position: double.tryParse(_currentPage.toString()) ?? 0,
                  decorator: DotsDecorator(
                    color: ColorHelper.grey, // Inactive color
                    activeColor: ColorHelper.primaryColor,
                    size: const Size.square(9.0),
                    activeSize: const Size(18.0, 9.0),
                    activeShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                  ),
                )),
              ),
              Expanded(
                child: isLastPage
                    ? PageViewButton(
                        title: 'Done',
                        onPressed: widget.onDone,
                      )
                    : PageViewButton(
                        isNext: true,
                        onPressed: !_isScrolling ? next : null,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PageViewButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  final bool isNext;

  const PageViewButton(
      {Key key, this.title, this.onPressed, this.isNext = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      child: isNext
          ? const Icon(Icons.chevron_right, color: ColorHelper.primaryColor)
          : Text(title ?? '', style: GenericAssets.buttonOrangeText),
    );
  }
}
