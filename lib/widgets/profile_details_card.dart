import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class ProfileDetailsCard extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function onPressed;

  const ProfileDetailsCard({Key key, this.title, this.icon, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Center(
        child: Padding(
          padding: GenericAssets.paddingSymV5,
          child: FractionallySizedBox(
            widthFactor: 0.95,
            alignment: Alignment.center,
            child: RaisedButton(
              padding: GenericAssets.padding10,
              color: ColorHelper.white,
              onPressed: onPressed,
              elevation: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: GenericAssets.paddingRight5,
                      child: Icon(icon,
                          color: ColorHelper.primaryColor, size: 34.0),
                    ),
                    Expanded(
                      child: Text(
                        title,
                        style: GenericAssets.defaultTextBlack,
                      ),
                    )
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
