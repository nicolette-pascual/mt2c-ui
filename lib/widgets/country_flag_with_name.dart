import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class CountryWidget extends StatelessWidget {
  final Country country;
  final String countryIsoCode;
  final String customName;
  final TextStyle style;
  final double flagHeight;
  final double flagWidth;

  const CountryWidget(
      {Key key,
      this.country,
      this.countryIsoCode,
      this.customName,
      this.style,
      this.flagHeight,
      this.flagWidth})
      : super(key: key);

  _getCountryFlag() {
    String _imagePath;
    _imagePath = CountryPickerUtils.getFlagImageAssetPath(
        countryIsoCode ?? country.isoCode);
    return _imagePath;
  }

  Widget _getFlagImage() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: Image.asset(
        _getCountryFlag(),
        height: flagHeight ?? 30,
        width: flagWidth ?? 40,
        fit: BoxFit.fill,
        package: _getCountryFlag() != null ? "country_pickers" : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        _getFlagImage(),
        Padding(
          padding: GenericAssets.paddingLeft5,
          child: Text(customName ?? country.name,
              style: style ?? GenericAssets.defaultTextBlueBold),
        )
      ],
    );
  }
}
