import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/country_flag_with_name.dart';
import 'package:mt2c_ui/widgets/form_fields/customized_material_dropdown.dart';

class CurrencyDropdown extends StatelessWidget {
  final String label;
  final List<Currency> values;
  final dynamic selectedValue;
  final Function onChanged;

  CurrencyDropdown(
      {@required this.values,
      this.selectedValue,
      @required this.onChanged,
      this.label});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: GenericAssets.paddingSymV5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FormField<Currency>(
            builder: (FormFieldState<Currency> state) {
              return Padding(
                padding: GenericAssets.paddingTop20,
                child: CustomDropdownButtonHideUnderline(
                  child: ButtonTheme(
                    alignedDropdown: true,
                    padding: GenericAssets.paddingNone,
                    child: CustomDropdownButton<Currency>(
                      icon: Icon(Icons.keyboard_arrow_down),
                      iconEnabledColor: ColorHelper.primaryColor,
                      value: selectedValue,
                      isDense: true,
                      onChanged: onChanged,
                      items: values.map((value) {
                        return CustomDropdownMenuItem(
                            value: value,
                            child: CountryWidget(
                                customName: value.currencyName,
                                countryIsoCode: value.isoCode,
                                style: GenericAssets
                                    .defaultText) //Text(value.currencyName),
                            );
                      }).toList(),
                    ),
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
