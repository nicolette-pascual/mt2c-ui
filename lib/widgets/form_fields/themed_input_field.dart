import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class ThemedInputField extends StatefulWidget {
  final bool autoFocus;
  final bool isRequired;
  final InputDecoration decoration;
  final FocusNode focusNode;
  final FocusNode nextFocusField;
  final Function onTap;
  final Function customValidations;
  final Function errorCallback;
  final Function onFieldSubmitted;
  final EdgeInsets padding;
  final String fieldName;
  final String prefixText;
  final String hintText;
  final TextStyle textStyle;
  final TextStyle labelTextStyle;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final TextCapitalization textCapitalization;
  final TextInputAction textInputAction;
  final Widget prefix;
  final Widget suffix;
  final bool useDefaultContentPadding;

  const ThemedInputField(
      {Key key,
      this.autoFocus,
      this.isRequired = false,
      this.decoration,
      this.focusNode,
      this.nextFocusField,
      this.onTap,
      this.customValidations,
      this.errorCallback,
      this.onFieldSubmitted,
      this.padding,
      this.fieldName,
      this.prefixText,
      this.hintText,
      this.textStyle,
      this.labelTextStyle,
      this.controller,
      this.keyboardType = TextInputType.text,
      this.textCapitalization,
      this.textInputAction,
      this.prefix,
      this.suffix,
      this.useDefaultContentPadding = false})
      : assert(fieldName != null, controller != null),
        super(key: key);

  @override
  _ThemedInputFieldState createState() => _ThemedInputFieldState();
}

class _ThemedInputFieldState extends State<ThemedInputField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: widget.padding ?? GenericAssets.padding15,
      child: TextFormField(
        style: widget.textStyle ?? GenericAssets.inputText,
        cursorColor: ColorHelper.primaryColor,
        focusNode: widget.focusNode,
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        textInputAction: widget.textInputAction ?? TextInputAction.done,
        onFieldSubmitted: widget.onFieldSubmitted ??
            (val) {
              widget.focusNode?.unfocus();
              if (widget.textInputAction == TextInputAction.next) {
                FocusScope.of(context).requestFocus(widget.nextFocusField);
              }
            },
        decoration: InputDecoration(
            contentPadding: widget.useDefaultContentPadding
                ? null
                : GenericAssets.paddingNone,
            labelText: widget.fieldName,
            labelStyle: GenericAssets.hintLabel,
            border: InputBorder.none,
            enabledBorder: InputBorder.none,
            prefixText: widget.prefixText ?? ''),
      ),
    );
  }
}
