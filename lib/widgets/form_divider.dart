import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';

class FormDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: ColorHelper.white,
        child: Divider(height: 2, indent: 10, endIndent: 10));
  }
}
