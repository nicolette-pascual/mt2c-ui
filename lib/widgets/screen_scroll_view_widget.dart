import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';

class ScreenScrollViewWidget extends StatelessWidget {
  final Widget child;
  final ScrollController scrollController;
  final EdgeInsets customPadding;
  final bool shouldUseIntrinsicHeight;

  const ScreenScrollViewWidget(
      {Key key,
      @required this.child,
      this.scrollController,
      this.customPadding,
      this.shouldUseIntrinsicHeight = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          padding: customPadding != null
              ? customPadding
              : GenericAssets.formContainerPadding,
          controller: scrollController,
          child: ConstrainedBox(
            constraints: BoxConstraints(
                minWidth: constraints.maxWidth,
                minHeight: constraints.maxHeight),
            child: shouldUseIntrinsicHeight
                ? IntrinsicHeight(child: child)
                : child,
          ),
        );
      },
    );
  }
}
