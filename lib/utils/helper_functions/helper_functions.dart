class HelperFunctions {
  static double calculateFee(double amount) {
    double fee;
    double _amt = amount ?? 0.0;
    if (_amt <= 1000) {
      fee = amount * 0.02;
    } else if (_amt > 1000 && _amt <= 10000) {
      fee = amount * 0.01;
    } else if (_amt > 10000) {
      fee = 0.0;
    } else {
      fee = null;
    }
    return fee;
  }
}
