import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:mt2c_ui/screens/amount_and_commission.dart';
import 'package:mt2c_ui/screens/commission_calculation.dart';
import 'package:mt2c_ui/screens/destinations.dart';
import 'package:mt2c_ui/screens/home.dart';
import 'package:mt2c_ui/screens/instructions_overview.dart';
import 'package:mt2c_ui/screens/menu/dashboard_overview.dart';
import 'package:mt2c_ui/screens/menu/transaction_detail.dart';
import 'package:mt2c_ui/screens/receiver_form.dart';
import 'package:mt2c_ui/screens/sms_verification.dart';
import 'package:mt2c_ui/screens/transfer_details.dart';
import 'package:mt2c_ui/screens/transfer_summary.dart';
import 'package:page_transition/page_transition.dart';

abstract class NamedRouting {
  String get routeName;
}

class NavigationHelper {
  static _buildRoute(RouteSettings settings, Widget builder) {
    bool _useCustomTransition;
    if (settings?.arguments != null) {
      final Map arguments = settings.arguments as Map;
      _useCustomTransition = arguments['useCustomTransition'];
    }
    return (_useCustomTransition ?? false)
        ? PageTransition(
            type: PageTransitionType.scale,
            settings: settings,
            alignment: Alignment.center,
            child: builder)
        : PageRouteBuilder(
            settings: settings,
            pageBuilder: (context, animation, secondaryAnimation) {
              return builder;
            },
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return Align(
                child: FadeTransition(
                  opacity: animation,
                  child: child,
                ),
              );
            },
          );
  }

  static Route getRoutes(RouteSettings settings) {
    switch (settings.name) {
      case Home.routeName:
        return _buildRoute(settings, Home());
      case InstructionsOverviewScreen.routeName:
        return _buildRoute(settings, InstructionsOverviewScreen());
      case DashboardOverviewScreen.routeName:
        return _buildRoute(settings, DashboardOverviewScreen());
      case TransactionDetailScreen.routeName:
        return _buildRoute(settings, TransactionDetailScreen());
      case DestinationScreen.routeName:
        return _buildRoute(settings, DestinationScreen());
      case AmountAndCommissionScreen.routeName:
        return _buildRoute(settings, AmountAndCommissionScreen());
      case CommissionCalculationScreen.routeName:
        return _buildRoute(settings, CommissionCalculationScreen());
      case SmsVerificationScreen.routeName:
        return _buildRoute(settings, SmsVerificationScreen());
      case ReceiverFormScreen.routeName:
        return _buildRoute(settings, ReceiverFormScreen());
      case TransferSummaryScreen.routeName:
        return _buildRoute(settings, TransferSummaryScreen());
      case TransferDetailsScreen.routeName:
        return _buildRoute(settings, TransferDetailsScreen());
      default:
        return null;
    }
  }

  static goToHome(BuildContext context) => Navigator.of(context)
      .pushNamedAndRemoveUntil(Home.routeName, (Route<dynamic> route) => false);

  static dynamic popUntil(BuildContext context, String toPage, results) {
    if (results is PopWithResults) {
      PopWithResults popResult = results;
      if (popResult.toPage == toPage) {
        return popResult.results;
      } else {
        // pop to previous page
        Navigator.of(context).pop(results);
      }
    }
  }
}

/// PopResult
class PopWithResults<T> {
  /// pop until this page
  final String toPage;

  /// results
  final Map<String, T> results;

  /// constructor
  PopWithResults({@required this.toPage, this.results});
}
