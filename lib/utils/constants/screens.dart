class Screens {
  static const String SPLASH_SCREEN = '/splash_screen';
  static const String AUTH_HOME = '/';
  static const String HOME = '/home';

  static const String WELCOME = '/welcome';
  static const String DASHBOARD = '/dashboard';
  static const String TRANSACTION_DETAILS = '/transaction_details';

  static const String DESTINATION = '/destination';
  static const String AMOUNT_AND_COMMISSION = '/amount_and_commission';
  static const String COMMISSION_CALCULATION = '/commission_calculation';
  static const String SMS_VERIFICATION = '/sms_verification';
  static const String NEW_RECEIVER_FORM = '/new_receiver_form';
  static const String TRANSFER_SUMMARY = '/transfer_summary';
  static const String TRANSFER_DETAILS = '/transfer_details';
}
