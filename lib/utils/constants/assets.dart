class Assets {
  static const String LOGO_NAME = 'images/logo.png';
  static const String HOME_BACKGROUND = 'images/home-bg.png';
  static const String ONLINE_ICON = 'images/order_online_icon.png';
  static const String CASH_ICON = 'images/cash_icon.png';
  static const String ATM_ICON = 'images/atm_icon.png';
  static const String PAPER_BG = 'images/edged_img.png';
}
