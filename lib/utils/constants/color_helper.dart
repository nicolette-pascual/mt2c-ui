import 'package:flutter/material.dart';

class ColorHelper {
  static const Color backgroundColor = backgroundOrange;
  static const Color primaryColor = orange1;
  static const Color accentColor = orange1;
  static const Color datePickerAccentColor = orange1;
  static const Color iconColor = orange1;

  static const Color defaultText = defaultTextColor;
  static const Color defaultTextBlack = Colors.black87;
  static const Color inputText = Color(0xff4c4b47);
  static const Color defaultLabel = defaultTextColor;
  static const Color dialogTitleGray = defaultTextColor;
  static const Color defaultScaffoldColor = backgroundOrange;
  static const Color dialogButtonText =
      Color.fromRGBO(50, 122, 202, 1); //Based from screenshot provided
  static const Color listItemAlternatedBackground =
      Color(0x44E0E0E0); //needs to be transparent for inkwell effect

  static const Color backgroundOrange = Color(0xfffff3e5);
  static const Color orange1 = Color(0xffff8802);
  static const Color orange2 = Color.fromRGBO(238, 126, 6, 0.6);
  static const Color blue1 = Color(0xff253d5f);
  static const Color blue2 = Color(0xff253c61);
  static const Color blue3 = Color(0xff23304e); // Country Lists
  static const Color blue4 = Color(0xff42618c); // AppBar title
  static const Color blue5 = Color(0xff435d92); // Instructions Body
  static const Color darkGreen = Color(0xff33391f); // Instructions Title
  static const Color customGrey1 = Color(0xff908e88);
  static const Color customGrey2 = Color(0xff5b6e87); // Home Page subheader

  static const Color defaultTextColor = Color.fromRGBO(37, 61, 95, 1);
  static const Color transparent = Colors.transparent;
  static const Color white = Colors.white;
  static const Color black = Colors.black;
  static const Color grey = Colors.grey;
  static const Color grey100 = Color(0xFFF5F5F5);
  static const Color grey200 = Color(0xFFEEEEEE);
  static const Color grey300 = Color(0xFFE0E0E0);
  static const Color grey400 = Color(0xFFEBEBEB);
  static const Color loadingIndicatorBg = Colors.black26;
  static const Color blackTransparent10 = Color.fromRGBO(0, 0, 0, 0.1);
  static const Color blackTransparent60 = Color.fromRGBO(0, 0, 0, 0.6);
  static const Color blackTransparent40 = Color.fromRGBO(0, 0, 0, 0.4);
  static const Color blackTransparent20 = Color.fromRGBO(0, 0, 0, 0.2);
  static const Color whiteTransparent60 = Color.fromRGBO(255, 255, 255, 0.6);

  static const Color disabledGrey = Color.fromRGBO(250, 250, 250, 0);

  static const Color passwordRed = Color.fromRGBO(205, 50, 64, 1);
  static const Color fieldErrorRed = Color.fromRGBO(255, 214, 214, 1);

  static const Color transactionAmountPending = Colors.amber;
  static const Color transactionAmountSuccess = Colors.green;
  static const Color transactionAmountDenied = Colors.red;
  static const Color transactionAmountCanceled = Colors.grey;

  static const Color dialogErrorIcon = Color.fromRGBO(205, 33, 45, 1);
  static const Color templatesBackground = Color.fromRGBO(245, 245, 245, 1);

  static const Color genericError = Color(0xFFD50000);
  static const Color genericSuccess = Colors.green;
  static const Color genericInfo = Colors.blue;
  static const Color genericDisabled = Colors.grey;

  static const Color cameraRed = Colors.redAccent;
  static const Color cameraBlue = Colors.blue;
  static const Color cameraDisabled = Colors.grey;
  static const Color cameraBackground = Colors.black;

  static const Color uploadingErrorColor = Color(0xFFD6352E);
  // Dashboard
  static const Color iconBorderColor = Color.fromRGBO(205, 205, 205, 1);
  static const Color grayIconColor = Color.fromRGBO(85, 85, 85, 1);
  static const Color grayBorderColor = Color.fromRGBO(230, 230, 230, 1);
}
