import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';

class GenericAssets {
  // NEW STYLES
  static double mainContentWidth = 360;

  // 360 per field * 2 + 40 middle spacing + 60 horizontal padding
  static double mainContent2ColumnsWidth = 820;
  static double buttonWidth = 360;
  static double oneColumnSideContainerWidth = 290;

  // TEXT SIZES
  static const double TEXT_SMALL = 14;
  static const double TEXT_MEDIUM = 18; //default
  static const double TEXT_LARGE = 26;
  static const double TEXT_XLARGE = 35;
  static const double TEXT_XXLARGE = 45;
  static const double TEXT_XXXLARGE = 70;

  static TextStyle titleDefault = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_LARGE,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w900));
  static TextStyle titleDefaultBlack = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_LARGE,
      color: ColorHelper.black,
      fontWeight: FontWeight.w700));
  static TextStyle smallTitleDefault = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w400));
  static TextStyle smallTitleDefaultBlack = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.black,
      fontWeight: FontWeight.w700));
  static TextStyle xxLargeTitleDefault = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XXLARGE,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w400));
  static TextStyle defaultText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      fontWeight: FontWeight.w400,
      color: ColorHelper.defaultText));

  static TextStyle smallTextOrange = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      fontWeight: FontWeight.w400,
      color: ColorHelper.primaryColor));
  static TextStyle smallTextOrangeUnderline = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      fontWeight: FontWeight.w400,
      decoration: TextDecoration.underline,
      color: ColorHelper.primaryColor));
  static TextStyle smallTitleGray = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.grey,
      fontWeight: FontWeight.w400));
  static TextStyle smallTitleBlue = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.blue5,
      fontWeight: FontWeight.w400));

  static TextStyle appBarTitle = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.blue4,
      fontWeight: FontWeight.w400));

  static TextStyle defaultTextBlueBold = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.blue2,
      fontWeight: FontWeight.w800));
  static TextStyle defaultTextBlueXBold = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.blue2,
      fontWeight: FontWeight.w900));
  static TextStyle defaultTextBlue4 = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.blue4,
      fontWeight: FontWeight.w400));
  static TextStyle defaultTextBlue4Bold = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.blue4,
      fontWeight: FontWeight.w800));
  static TextStyle defaultTitleGray = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.grey,
      fontWeight: FontWeight.w400));
  static TextStyle defaultTextGreenBold = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.darkGreen,
      fontWeight: FontWeight.w700));
  static TextStyle defaultTextLightGreen = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.genericSuccess,
      fontWeight: FontWeight.w400));
  static TextStyle defaultTextBlack = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      fontWeight: FontWeight.w400,
      color: ColorHelper.black));

  static TextStyle homePageHeaderTitle = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XLARGE,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w900));
  static TextStyle homePageHeaderBody = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w300));

  static TextStyle instructionTitle = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.darkGreen,
      fontWeight: FontWeight.w800));
  static TextStyle instructionBody = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.blue5,
      fontWeight: FontWeight.w300));

  static TextStyle amountTransparentText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XXLARGE,
      decoration: TextDecoration.none,
      color: ColorHelper.transparent,
      fontWeight: FontWeight.w600));
  static TextStyle amountText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XXXLARGE,
      decoration: TextDecoration.none,
      color: ColorHelper.primaryColor,
      fontWeight: FontWeight.w600));
  static TextStyle amountSuffixText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XLARGE,
      color: ColorHelper.primaryColor,
      fontWeight: FontWeight.w400));

  static TextStyle xLargeBlue1 = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XLARGE,
      color: ColorHelper.blue1,
      fontWeight: FontWeight.w400));
  static TextStyle xLargeOrange1 = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XLARGE,
      color: ColorHelper.primaryColor,
      fontWeight: FontWeight.w400));

  static TextStyle inputText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w500,
      color: ColorHelper.inputText));
  static const TextStyle inputTextExtraBold = TextStyle(
    fontSize: TEXT_MEDIUM,
    fontWeight: FontWeight.w400,
    color: ColorHelper.defaultText,
  );
  static TextStyle inputTextWhite = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w300,
      color: Colors.white));
  static TextStyle inputTextDisabled = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w300,
      color: ColorHelper.blackTransparent40));
  static TextStyle whiteButtonSub = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      height: 1,
      fontWeight: FontWeight.w200,
      color: ColorHelper.defaultTextBlack));
  static TextStyle inputLabelText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM, fontWeight: FontWeight.w300, color: Colors.white));
  static TextStyle inputErrorText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      height: 1,
      fontWeight: FontWeight.w400,
      color: Colors.red));
  static TextStyle inputTextLargeDarkGrey =
      TextStyle(fontSize: GenericAssets.TEXT_LARGE, color: Colors.black54);

  static TextStyle buttonText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w700,
      color: Colors.white));
  static TextStyle buttonOrangeText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w700,
      color: ColorHelper.primaryColor));
  static TextStyle buttonTextDisabled = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XLARGE,
      height: 1,
      fontWeight: FontWeight.w300,
      color: ColorHelper.white));

  static TextStyle screenTitleText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_LARGE,
      fontWeight: FontWeight.w400,
      color: ColorHelper.defaultText));
  static TextStyle screenInstructionText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      fontWeight: FontWeight.w100,
      color: ColorHelper.defaultText,
      height: 1.20));

  static const TextStyle accessCodeText = TextStyle(
      fontSize: TEXT_XXLARGE,
      fontWeight: FontWeight.w600,
      color: ColorHelper.white);
  static const TextStyle numpadButton =
      TextStyle(fontSize: TEXT_XLARGE, fontWeight: FontWeight.w600);
  static const TextStyle radioButtonText = TextStyle(
      fontSize: TEXT_MEDIUM,
      color: ColorHelper.white,
      fontWeight: FontWeight.w600);

  static TextStyle totalPayLabel = wrapWithGoogleFont(
      TextStyle(fontSize: TEXT_XLARGE, fontWeight: FontWeight.w400));
  static TextStyle headerText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_LARGE,
      height: 1,
      fontWeight: FontWeight.w400,
      color: Colors.white));
  static TextStyle whiteButtonText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_SMALL,
      fontWeight: FontWeight.w900,
      color: ColorHelper.white));
  static TextStyle whiteButtonTextDisabled = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_LARGE,
      fontWeight: FontWeight.w200,
      color: ColorHelper.grey));
  static TextStyle promptText = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      fontWeight: FontWeight.w400,
      color: ColorHelper.grayIconColor));
  static TextStyle hintLabel = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w400,
      color: ColorHelper.grey300));
  static TextStyle inputFieldPlaceholder = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_MEDIUM,
      height: 1,
      fontWeight: FontWeight.w200,
      color: ColorHelper.grey));

  // Generic TextStyles
  static const TextStyle topbarTitle =
      TextStyle(fontSize: TEXT_SMALL, fontWeight: FontWeight.bold);

  static const TextStyle dialogText = TextStyle(fontSize: TEXT_MEDIUM);
  static const TextStyle dialogTextBlack =
      TextStyle(fontSize: TEXT_MEDIUM, color: ColorHelper.defaultTextBlack);
  static const TextStyle dialogTitle = TextStyle(
      fontSize: TEXT_LARGE,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w400);
  static const TextStyle dialogTitleBlack = TextStyle(
      fontSize: TEXT_MEDIUM,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      color: ColorHelper.defaultTextBlack);
  static const TextStyle skipText = TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.white,
      fontWeight: FontWeight.w300);
  static const TextStyle textTimer = TextStyle(
      fontSize: TEXT_SMALL,
      color: ColorHelper.defaultText,
      fontWeight: FontWeight.w200);

  //Default text with minor changes
  static const TextStyle defaultTextSmallBold = TextStyle(
      fontWeight: FontWeight.w400,
      color: ColorHelper.defaultText,
      fontSize: TEXT_SMALL);
  static const TextStyle defaultTextSmallOrangeBold = TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: TEXT_SMALL,
      color: ColorHelper.primaryColor);

  //DISABLED COLOR (light gray) to determine disabled texts on buttons
  static const TextStyle defaultTextDisabled = TextStyle(
      fontWeight: FontWeight.w200, color: ColorHelper.genericDisabled);

  static TextStyle transferId = wrapWithGoogleFont(TextStyle(
      fontSize: TEXT_XXXLARGE, fontWeight: FontWeight.w600, height: 1.25));
  static const TextStyle transferIdLabel = TextStyle(
    color: Colors.white,
    fontSize: TEXT_XLARGE,
  );

  static const EdgeInsets inputLabelPadding =
      EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5);
  static const EdgeInsets topBarPadding =
      EdgeInsets.symmetric(horizontal: 30.0, vertical: 20);

  // Horizontal padding for 2-column forms
  static const EdgeInsets horizontalPadding30 =
      EdgeInsets.symmetric(horizontal: 30.0);
  static const EdgeInsets paddingLeft25 = EdgeInsets.only(left: 25);
  static const EdgeInsets paddingRight20 = EdgeInsets.only(right: 20);
  static const EdgeInsets datePickerPadding =
      EdgeInsets.symmetric(vertical: 150, horizontal: 290);
  static const EdgeInsets hintTopPadding = EdgeInsets.only(top: 35);
  static const EdgeInsets paddingNone = EdgeInsets.all(0.0);
  static const EdgeInsets hintTextPadding =
      EdgeInsets.symmetric(horizontal: 15, vertical: 10);
  static const EdgeInsets snackbarButtonPadding =
      EdgeInsets.symmetric(horizontal: 20, vertical: 10);
  static const EdgeInsets paddingRight10 = EdgeInsets.only(right: 10.0);

  // BORDER RADIUS FOR NEW STYLES
  static const BorderRadius borderRadiusAll20 =
      BorderRadius.all(Radius.circular(20));
  static const BorderRadius borderRadiusAll10 =
      BorderRadius.all(Radius.circular(10));

  static const OutlineInputBorder disabledFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(15.0)),
      borderSide: BorderSide(width: 0.0, style: BorderStyle.none));

  //FIXME: cleanup styles below

  // GENERIC STYLES USED
  // By default all padding horizontally and vertically are 20.00
  // unless on special circumstances that needs specific padding on each widgets
  static const EdgeInsets formContainerPadding = paddingSymH15;

  // top: 100 = 60 as current appbar height + 40
  static const EdgeInsets screenPadding =
      EdgeInsets.only(top: 30, left: 40, right: 40, bottom: 20);
  static const EdgeInsets transferSummaryScreenPadding =
      EdgeInsets.only(top: 40, left: 40, right: 40, bottom: 10);
  static const EdgeInsets paymentSuccessScreenPadding =
      EdgeInsets.only(top: 20, left: 40, right: 40, bottom: 10);

  // static const EdgeInsets screenPadding = EdgeInsets.all(40);
  static const EdgeInsets paddingTopField = EdgeInsets.only(bottom: 5);
  static const EdgeInsets paddingTopButton = EdgeInsets.only(top: 20);

  //TextformField Padding
  static const EdgeInsets textFormFieldSpacing =
      EdgeInsets.only(top: 15.0, right: 10.0, bottom: 5.0, left: 1.0);
  static const EdgeInsets isPepTextFormFieldSpacing =
      EdgeInsets.only(right: 10.0, bottom: 5.0, left: 10.0);

  // Generic EdgeInsets
  static const EdgeInsets padding5 = EdgeInsets.all(5.0);
  static const EdgeInsets padding8 = EdgeInsets.all(8.0);
  static const EdgeInsets padding10 = EdgeInsets.all(10.0);
  static const EdgeInsets padding15 = EdgeInsets.all(15.0);
  static const EdgeInsets padding20 = EdgeInsets.all(20.0);
  static const EdgeInsets padding30 = EdgeInsets.all(30.0);
  static const EdgeInsets paddingBottom5 = EdgeInsets.only(bottom: 5.0);
  static const EdgeInsets paddingBottom10 = EdgeInsets.only(bottom: 10.0);
  static const EdgeInsets paddingBottom15 = EdgeInsets.only(bottom: 15.0);
  static const EdgeInsets paddingBottom20 = EdgeInsets.only(bottom: 20.0);
  static const EdgeInsets paddingBottom24 = EdgeInsets.only(bottom: 24.0);
  static const EdgeInsets paddingBottom30 = EdgeInsets.only(bottom: 30.0);
  static const EdgeInsets paddingBottom40 = EdgeInsets.only(bottom: 40.0);
  static const EdgeInsets paddingBottom50 = EdgeInsets.only(bottom: 50.0);
  static const EdgeInsets paddingBottom70 = EdgeInsets.only(bottom: 70.0);
  static const EdgeInsets paddingTop5 = EdgeInsets.only(top: 5.0);
  static const EdgeInsets paddingTop10 = EdgeInsets.only(top: 10.0);
  static const EdgeInsets paddingTop15 = EdgeInsets.only(top: 15.0);
  static const EdgeInsets paddingTop20 = EdgeInsets.only(top: 20.0);
  static const EdgeInsets paddingTop25 = EdgeInsets.only(top: 25.0);
  static const EdgeInsets paddingTop50 = EdgeInsets.only(top: 50.0);
  static const EdgeInsets padding20Bottom5 =
      EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0, bottom: 5.0);
  static const EdgeInsets paddingLeft10 = EdgeInsets.only(left: 10.0);
  static const EdgeInsets paddingLeft15 = EdgeInsets.only(left: 15.0);
  static const EdgeInsets paddingLeft20 = EdgeInsets.only(left: 20.0);
  static const EdgeInsets paddingLeft40 = EdgeInsets.only(left: 40.0);
  static const EdgeInsets paddingLeft60 = EdgeInsets.only(left: 60.0);
  static const EdgeInsets paddingSym3 = EdgeInsets.symmetric(
    horizontal: 3.0,
  );
  static const EdgeInsets paddingSymV10H15 =
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0);
  static const EdgeInsets paddingSym15 = EdgeInsets.symmetric(
    horizontal: 15.0,
  );
  static const EdgeInsets paddingSymH10V5 =
      EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0);
  static const EdgeInsets errorMessagePadding =
      EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0);
  static const EdgeInsets paddingSymH10V20 =
      EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0);
  static const EdgeInsets paddingSymH30V5 =
      EdgeInsets.symmetric(horizontal: 30.0, vertical: 5.0);
  static const EdgeInsets paddingSymV5 = EdgeInsets.symmetric(vertical: 5.0);
  static const EdgeInsets paddingSymV10 = EdgeInsets.symmetric(vertical: 10.0);
  static const EdgeInsets paddingSymV15 = EdgeInsets.symmetric(vertical: 15.0);
  static const EdgeInsets paddingSymV24 = EdgeInsets.symmetric(vertical: 24.0);
  static const EdgeInsets paddingSymH15 =
      EdgeInsets.symmetric(horizontal: 10.0);
  static const EdgeInsets paddingSymH20 =
      EdgeInsets.symmetric(horizontal: 20.0);
  static const EdgeInsets paddingSymH30 =
      EdgeInsets.symmetric(horizontal: 30.0);

  static const EdgeInsets senderResultPadding =
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 15);

  //Custom for Access Code Screen
  static const EdgeInsets paddingH20Bottom20 =
      EdgeInsets.only(right: 20.0, bottom: 20.0, left: 20.0);
  static const EdgeInsets paddingR5T3 = EdgeInsets.only(right: 5.0, top: 3.0);
  static const EdgeInsets paddingBottom3 = EdgeInsets.only(bottom: 3.0);
  static const EdgeInsets paddingBottom3Left5 =
      EdgeInsets.only(bottom: 3, left: 5);
  static const EdgeInsets paddingRight5 = EdgeInsets.only(right: 5.0);
  static const EdgeInsets paddingLeft5 = EdgeInsets.only(left: 5.0);
  static const EdgeInsets paddingB10L5R10 =
      EdgeInsets.only(bottom: 10.0, left: 15.0, right: 10.0);

  static const EdgeInsets dialogContentPadding =
      EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 20.0);

  static const EdgeInsets paddingSymL5R20 = EdgeInsets.only(left: 5, right: 20);

  static const TextStyle dialogContent = TextStyle(
      color: ColorHelper.defaultTextBlack,
      fontSize: TEXT_SMALL,
      fontWeight: FontWeight.w400);
  static const TextStyle dialogButtonText = TextStyle(
      color: ColorHelper.dialogButtonText,
      fontSize: TEXT_MEDIUM,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500);

  // Border Radius
  static const BorderRadius borderRadius100 =
      BorderRadius.all(Radius.circular(100));
  static const BorderRadius borderRadius12 =
      BorderRadius.all(Radius.circular(12));
  static const BorderRadius borderRadius10 =
      BorderRadius.all(Radius.circular(10));

  // Navigation Button
  static const EdgeInsets navigationButtonPadding =
      EdgeInsets.symmetric(horizontal: 30, vertical: 25);

  static const RoundedRectangleBorder buttonBorderShape =
      RoundedRectangleBorder(borderRadius: borderRadius10);

  static const RoundedRectangleBorder backNavigationBorder =
      RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25)));
  static const RoundedRectangleBorder nextNavigationBorder =
      RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(25)));

  static getAppTheme(BuildContext context) {
    return ThemeData(
      brightness: Brightness.light,
      textSelectionHandleColor: ColorHelper.primaryColor,
      primaryColor: ColorHelper.primaryColor,
      accentColor: ColorHelper.accentColor,
      // fontFamily: 'IBM Plex Sans',
      iconTheme: IconThemeData(color: ColorHelper.iconColor),
      scaffoldBackgroundColor: ColorHelper.defaultScaffoldColor,
      appBarTheme: AppBarTheme(
          color: ColorHelper.white,
          textTheme: TextTheme(
              title: TextStyle(
                  color: ColorHelper.blue4,
                  fontSize: TEXT_MEDIUM,
                  fontWeight: FontWeight.w600)),
          iconTheme: IconThemeData(color: ColorHelper.primaryColor)),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: GenericAssets.hintLabel,
        contentPadding: padding10,
        filled: true,
        fillColor: Colors.white,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
      ),
      textTheme: GoogleFonts.latoTextTheme(TextTheme(
        title: titleDefault,
        body1: defaultText,
        body2: defaultText,
        subhead: defaultText,
        button: buttonText,
        display1: amountText,
      )),
      buttonTheme: ButtonThemeData(
          shape: buttonBorderShape,
          height: 50.0,
          buttonColor: ColorHelper.orange1,
          textTheme: ButtonTextTheme.primary),
      buttonBarTheme: ButtonBarThemeData(
          alignment: MainAxisAlignment.spaceEvenly,
          buttonTextTheme: ButtonTextTheme.accent),
      dialogTheme: DialogTheme(
        shape: OutlineInputBorder(borderRadius: borderRadiusAll10),
        titleTextStyle: dialogTitleBlack,
        contentTextStyle: dialogContent,
      ),
    );
  }

  static getDisabledFieldTheme(BuildContext context) {
    return ThemeData(
        inputDecorationTheme: InputDecorationTheme(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
          labelStyle: wrapWithGoogleFont(TextStyle(
              color: ColorHelper.white,
              fontSize: TEXT_SMALL,
              fontWeight: FontWeight.w400)),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: ColorHelper.primaryColor, width: 1.0),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: ColorHelper.primaryColor, width: 1.0),
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: ColorHelper.primaryColor, width: 1.0),
          ),
          suffixStyle: inputTextWhite,
        ),
        textTheme: GoogleFonts.ibmPlexSansTextTheme(TextTheme(
          title: inputTextWhite,
          body1: inputTextWhite,
          body2: inputTextWhite,
          subhead: inputTextWhite,
        )));
  }

  static getFlushbarButtonTheme(BuildContext context) {
    return ThemeData(
      buttonTheme: ButtonThemeData(
        minWidth: 20,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
      textTheme: GoogleFonts.ibmPlexSansTextTheme(TextTheme(
        button: buttonText,
      )),
    );
  }

  static getDatePickerTheme(BuildContext context) {
    return ThemeData(
      primarySwatch: datePickerSwatchColor,
      primaryColor: ColorHelper.primaryColor,
      accentColor: ColorHelper.datePickerAccentColor,
    );
  }

  static const MaterialColor datePickerSwatchColor = const MaterialColor(
    0xff369d4b,
    const <int, Color>{
      50: const Color(0xff369d4b),
      100: const Color(0xff369d4b),
      200: const Color(0xff369d4b),
      300: const Color(0xff369d4b),
      400: const Color(0xff369d4b),
      500: const Color(0xff369d4b),
      600: const Color(0xff369d4b),
      700: const Color(0xff369d4b),
      800: const Color(0xff369d4b),
      900: const Color(0xff369d4b),
    },
  );

  static wrapWithGoogleFont(TextStyle style) {
    return GoogleFonts.lato(textStyle: style);
  }
}
