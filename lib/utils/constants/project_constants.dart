import 'package:intl/intl.dart';
import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/models/transaction.dart';

enum TransferType { incoming, outgoing }

class ProjectConstants {
  static final double formPaddingWithAppBar = 115;
  static final double appBarHeight = 60.00;

  static final int smsTimerDuration = 60; // in seconds

  static final NumberFormat amountFormat = NumberFormat("#,###");

  static final List<Country> favoriteCountries = [
    Country(isoCode: 'ua', name: 'Ukraine'),
    Country(isoCode: 'ru', name: 'Russian Federation'),
    Country(isoCode: 'by', name: 'Belarus'),
  ];

  static final List<Country> countries = [
    Country(isoCode: 'ua', name: 'Ukraine'),
    Country(isoCode: 'ru', name: 'Russian Federation'),
    Country(isoCode: 'by', name: 'Belarus'),
    Country(isoCode: 'cz', name: 'Czech Republic'),
    Country(isoCode: 'gb', name: 'United Kingdom'),
    Country(isoCode: 'us', name: 'United States'),
  ];

  static final List<Currency> currencies = [
    Currency(isoCode: 'cz', currencyName: 'CZK'),
    Currency(isoCode: 'ru', currencyName: 'RUB'),
    Currency(isoCode: 'us', currencyName: 'USD'),
    Currency(isoCode: 'gb', currencyName: 'GBP'),
  ];

  static final List<Transaction> transactions = [
    Transaction(
        name: 'Jan Novak',
        status: 'Awaiting confirmation',
        date: '30 March',
        amount: 200.21,
        currency: 'EUR',
        transferType: TransferType.outgoing),
    Transaction(
        name: 'Margarita Polevska',
        status: 'Incoming',
        date: '29 March',
        amount: 60,
        currency: 'CZK',
        transferType: TransferType.incoming),
    Transaction(
        name: 'Jan Novak',
        status: 'Incoming',
        date: '29 March',
        amount: 500.32,
        currency: 'CZK',
        transferType: TransferType.incoming),
  ];
}
