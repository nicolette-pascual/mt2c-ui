import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/models/currency.dart';

final CountryRepository countryRepository = CountryRepository();

class CountryRepository {
  static final CountryRepository _repository = CountryRepository._internal();

  factory CountryRepository() {
    return _repository;
  }

  CountryRepository._internal();

  Country _selectedCountry;
  Currency _currency;

  Country get getSelectedCountry => this._selectedCountry;

  set setSelectedCountry(Country country) => _selectedCountry = country;

  Currency get getCurrency => this._currency;

  set setCurrency(Currency currency) => _currency = currency;
}
