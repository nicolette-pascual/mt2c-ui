import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/repositories/country_repository.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';

class CountryBloc {
  List<Country> get favoriteCountryList => ProjectConstants.favoriteCountries;
  List<Country> get allCountryList => ProjectConstants.countries;
  List<Currency> get currencyList => ProjectConstants.currencies;

  Currency get currency => countryRepository.getCurrency;

  Country get selectedCountry => countryRepository.getSelectedCountry;

  submit(Country country) {
    countryRepository?.setSelectedCountry = country;
    Currency countryCurrency = currencyList.singleWhere(
        (item) => item.isoCode == country.isoCode,
        orElse: () => null);
    countryRepository?.setCurrency = countryCurrency ?? currencyList.first;
  }
}
