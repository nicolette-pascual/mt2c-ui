import 'package:flutter/material.dart';
import 'package:mt2c_ui/manager/country_manager.dart';
import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/country_flag_with_name.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/screen_title_instruction.dart';

class DestinationScreen extends StatefulWidget {
  static const routeName = Screens.DESTINATION;
  @override
  _DestinationScreenState createState() => _DestinationScreenState();
}

class _DestinationScreenState extends State<DestinationScreen> {
  List<Country> _countries;
  CountryBloc _countryBloc;
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  var _listItems = <Widget>[];

  @override
  void initState() {
    super.initState();
    _countryBloc = CountryBloc();
    _loadCountryList();
  }

  void _loadCountryList() {
    List fetchedList = [];
    _countries = _countryBloc.allCountryList;

    for (var i = 0; i < _countries.length; i++) {
      fetchedList.add(
        ListTile(
          title: CountryWidget(country: _countries[i]),
          onTap: () {
            _onNext(country: _countries[i]);
          },
          trailing: Icon(Icons.arrow_forward, color: ColorHelper.primaryColor),
        ),
      );
    }

    var future = Future(() {});
    for (var i = 0; i < fetchedList.length; i++) {
      future = future.then((_) {
        return Future.delayed(Duration(milliseconds: 300), () {
          _listItems.add(fetchedList[i]);
          _listKey.currentState.insertItem(_listItems.length - 1);
        });
      });
    }
  }

  _onNext({Country country}) {
    _countryBloc.submit(country);
    Navigator.of(context).pushNamed(Screens.AMOUNT_AND_COMMISSION);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: 'Destinations',
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: GenericAssets.paddingLeft10,
            child: Hero(
              tag: 'country',
              child: ScreenTitleInstructionWidget(
                title: 'Destination Country',
              ),
            ),
          ),
          Expanded(
            child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child: AnimatedList(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  key: _listKey,
                  initialItemCount: _listItems.length,
                  itemBuilder:
                      (BuildContext context, int index, Animation animation) {
                    return FadeTransition(
                      opacity: animation,
                      child: _listItems[index],
                    );
                  },
                )),
          ),
        ],
      ),
    );
  }
}
