import 'package:flutter/material.dart';
import 'package:mt2c_ui/manager/country_manager.dart';
import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/utils/constants/assets.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/country_flag_with_name.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/designs/slide_up_animation.dart';
import 'package:mt2c_ui/widgets/full_width_button.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class Home extends StatefulWidget {
  static const routeName = Screens.HOME;
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  CountryBloc _countryBloc;
  List<Country> _countries;
  List _listItems = <Widget>[];

  @override
  void initState() {
    super.initState();
    _countryBloc = CountryBloc();
    _loadCountryList();
  }

  _onNext({Country country, bool isFavorite = false}) {
    if (isFavorite) {
      _countryBloc.submit(country);
      Navigator.of(context).pushNamed(Screens.AMOUNT_AND_COMMISSION);
    } else {
      Navigator.of(context).pushNamed(Screens.DESTINATION);
    }
  }

  _onShowInstructions() {
    Navigator.of(context).pushNamed(Screens.WELCOME);
  }

  void _loadCountryList() {
    _countries = _countryBloc?.favoriteCountryList;
    _listItems = <Widget>[];
    for (var i = 0; i < _countries.length; i++) {
      _listItems.add(
        ListTile(
          title: CountryWidget(country: _countries[i]),
          onTap: () {
            _onNext(country: _countries[i], isFavorite: true);
          },
          trailing: Icon(Icons.arrow_forward, color: ColorHelper.primaryColor),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.5;
    return Scaffold(
        appBar: CustomAppBar(isHomePage: true),
        extendBodyBehindAppBar: true,
        body: ScreenScrollViewWidget(
          customPadding: GenericAssets.paddingNone,
          shouldUseIntrinsicHeight: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _getHeaderWithButton(),
              SizedBox(height: 40),
              Text('Send money to:', style: GenericAssets.smallTitleGray),
              Flexible(
                child: MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child: SingleChildScrollView(
                      child: Column(
                        children: _listItems,
                      ),
                    )),
              ),
              Padding(
                padding: GenericAssets.paddingSymH10V20,
                child: GestureDetector(
                  onTap: _onShowInstructions,
                  child: Text(
                    'How sending with ATM works?',
                    style: GenericAssets.smallTitleDefault,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Widget _getHeaderWithButton() {
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Image.asset(Assets.HOME_BACKGROUND),
        Positioned(
            top: 85,
            bottom: 50,
            left: 10,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SlideUpAnimation(
                  child: Text(
                    "Save Time.",
                    style: GenericAssets.homePageHeaderTitle,
                  ),
                  delay: 500,
                ),
                SlideUpAnimation(
                  child: Text(
                    "Create cash\ntransfers online.",
                    style: GenericAssets.homePageHeaderTitle,
                  ),
                  delay: 1000,
                ),
                SizedBox(height: 5),
                SlideUpAnimation(
                  child: Text('Use ATM to send cash\nto friends and family',
                      style: GenericAssets.homePageHeaderBody),
                  delay: 1500,
                ),
              ],
            )),
        Positioned(
          left: 10,
          child: Image.asset(
            Assets.LOGO_NAME,
            width: 120,
            height: 120,
          ),
        ),
        Positioned(
          right: 20,
          left: 20,
          bottom: -35,
          child: Hero(
            tag: 'country',
            child: FullWidthButton(
              'Choose a destination country',
              _onNext,
              widthPercentage: 0.4,
            ),
          ),
        )
      ],
    );
  }
}
