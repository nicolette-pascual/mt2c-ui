import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/page_view_model.dart';
import 'package:mt2c_ui/models/transaction.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/overview.dart';
import 'package:mt2c_ui/widgets/profile_details_card.dart';

class DashboardOverviewScreen extends StatefulWidget {
  static const routeName = Screens.DASHBOARD;
  @override
  _DashboardOverviewScreenState createState() =>
      _DashboardOverviewScreenState();
}

class _DashboardOverviewScreenState extends State<DashboardOverviewScreen> {
  List<Transaction> _transactions = ProjectConstants.transactions;
  List _awaitingTransList = <Widget>[];
  List _allTransList = <Widget>[];

  @override
  void initState() {
    super.initState();
    _loadAwaitingTransactions();
  }

  void _loadAwaitingTransactions() {
    _awaitingTransList.clear();
    _allTransList.clear();
    for (var i = 0; i < _transactions.length; i++) {
      _awaitingTransList.add(_buildListTile(_transactions[i], true));
      _allTransList.add(_buildListTile(_transactions[i], false));
    }
  }

  _onTransferTileTap(Transaction transaction) {
    Navigator.of(context).pushNamed(Screens.TRANSACTION_DETAILS,
        arguments: {'transaction': transaction, 'useCustomTransition': true});
  }

  Widget _buildListTile(Transaction transaction, bool isAwaitingTrans) {
    return ListTile(
      leading: Container(
        decoration: BoxDecoration(
          border: Border.all(color: ColorHelper.backgroundColor, width: 4.0),
          color: ColorHelper.backgroundColor,
          shape: BoxShape.circle,
        ),
        child: Padding(
          padding: GenericAssets.padding5,
          child: Icon(
              (transaction.transferType == TransferType.incoming)
                  ? Icons.call_received
                  : Icons.call_made,
              color: ColorHelper.primaryColor,
              size: 30),
        ),
      ),
      title: Text(transaction.name, style: GenericAssets.defaultTextBlack),
      subtitle: Text(isAwaitingTrans ? transaction.status : transaction.date,
          style: GenericAssets.smallTitleGray),
      onTap: () {
        _onTransferTileTap(transaction);
      },
      trailing: Container(
        height: double.infinity,
        child: Padding(
          padding: GenericAssets.paddingTop5,
          child: Text(
              transaction.amount.toString() + '\t' + transaction.currency,
              style: GenericAssets.defaultTextBlack),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorHelper.white,
        body: Overview(
            useDashboardLayout: true,
            pages: [
              PageViewModel(
                  title: "Transactions",
                  bodyWidget: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: GenericAssets.paddingSymH15,
                        child: Text('Awaiting details of Confirmation',
                            style: GenericAssets.smallTitleDefaultBlack),
                      ),
                      Divider(indent: 10, endIndent: 10),
                      Flexible(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: _awaitingTransList,
                        ),
                      ),
                      Padding(
                          padding: GenericAssets.paddingTop25,
                          child: Padding(
                            padding: GenericAssets.paddingSymH15,
                            child: Text('All Transactions',
                                style: GenericAssets.smallTitleDefaultBlack),
                          )),
                      Divider(indent: 10, endIndent: 10),
                      Flexible(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: _allTransList,
                        ),
                      ),
                    ],
                  )),
              PageViewModel(
                  title: "Profile",
                  bodyWidget: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: GenericAssets.paddingSymH15,
                        child: Text('Account Details',
                            style: GenericAssets.smallTitleDefaultBlack),
                      ),
                      Divider(indent: 10, endIndent: 10),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ProfileDetailsCard(
                              title: 'Personal Details',
                              icon: Icons.account_box,
                              onPressed: () {},
                            ),
                            ProfileDetailsCard(
                              title: 'Verification',
                              icon: Icons.verified_user,
                              onPressed: () {},
                            ),
                            ProfileDetailsCard(
                              title: 'Password',
                              icon: Icons.vpn_key,
                              onPressed: () {},
                            )
                          ],
                        ),
                      ),
                    ],
                  )),
            ],
            onDone: null,
            onSkip: null));
  }
}
