import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/transaction.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';

class TransactionDetailScreen extends StatefulWidget {
  static const routeName = Screens.TRANSACTION_DETAILS;
  @override
  _TransactionDetailScreenState createState() =>
      _TransactionDetailScreenState();
}

class _TransactionDetailScreenState extends State<TransactionDetailScreen> {
  Transaction _transaction;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (ModalRoute.of(context).settings?.arguments != null) {
        final Map arguments = ModalRoute.of(context).settings.arguments as Map;
        setState(() {
          _transaction = arguments['transaction'];
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: CustomAppBar(),
      backgroundColor: ColorHelper.white,
      body: ScreenScrollViewWidget(
        shouldUseIntrinsicHeight: true,
        customPadding: GenericAssets.paddingNone,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _getInfoFields('Date and Type:', _getDateAndType()),
            _getInfoFields('Details:', _getDetails(size)),
            _getInfoFields('Amount', _getAmount(size), showDivider: false),
            Spacer(flex: 8),
          ],
        ),
      ),
    );
  }

  Widget _getDateAndType() {
    String _transferType;

    if (_transaction?.transferType == TransferType.incoming) {
      _transferType = 'Incoming Wire';
    } else {
      _transferType = 'Outward Wire';
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text((_transaction?.date ?? '') + ' 2020'),
        Text(_transferType ?? '', style: GenericAssets.defaultTitleGray)
      ],
    );
  }

  Widget _getDetails(size) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text('Beneficiary:', style: GenericAssets.smallTitleBlue),
            SizedBox(height: 10),
            Text('Originator:', style: GenericAssets.smallTitleBlue),
          ],
        ),
        SizedBox(width: size.width * 0.05),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(_transaction?.name ?? ''),
            Text("MoneyPolo A.S"),
            Text("In Bank Transfer feeSP112347356",
                style: const TextStyle(fontSize: 9)),
            Text("ACC.AGR?9311.19? TO OWN ACC CZ-ABC-331237;",
                style: const TextStyle(fontSize: 9)),
          ],
        )
      ],
    );
  }

  Widget _getAmount(size) {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Currency:', style: GenericAssets.smallTitleBlue),
            Padding(padding: GenericAssets.paddingTop10, child: Text('Amount:'))
          ],
        ),
        SizedBox(width: size.width * 0.05),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(_transaction?.currency ?? ''),
            Padding(
              padding: GenericAssets.paddingTop5,
              child: Text(
                  "${_transaction?.amount?.toString()} ${_transaction?.currency}",
                  style: GenericAssets.defaultTextBlue4Bold),
            )
          ],
        )
      ],
    );
  }

  Widget _getInfoFields(String title, Widget info,
      {bool showDivider = true, Widget spacer}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: GenericAssets.paddingSymV10H15,
          child: Text(title ?? '', style: GenericAssets.defaultTextBlueBold),
        ),
        Padding(
          padding: GenericAssets.paddingLeft20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  spacer ?? Container(),
                  info,
                ],
              ),
              showDivider
                  ? Padding(
                      padding: GenericAssets.paddingTop15,
                      child: Divider(endIndent: 15),
                    )
                  : Container()
            ],
          ),
        )
      ],
    );
  }
}
