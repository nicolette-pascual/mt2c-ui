import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mt2c_ui/manager/country_manager.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/utils/helper_functions/helper_functions.dart';
import 'package:mt2c_ui/widgets/amount_field.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/designs/animated_wave_background.dart';
import 'package:mt2c_ui/widgets/designs/cross_fade_text.dart';
import 'package:mt2c_ui/widgets/full_width_button.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';
import 'package:mt2c_ui/widgets/screen_title_instruction.dart';

class AmountAndCommissionScreen extends StatefulWidget {
  static const routeName = Screens.AMOUNT_AND_COMMISSION;
  @override
  _AmountAndCommissionScreenState createState() =>
      _AmountAndCommissionScreenState();
}

class _AmountAndCommissionScreenState extends State<AmountAndCommissionScreen> {
  Timer timer;
  TextEditingController _amountController;
  CountryBloc _countryBloc;
  Currency _currency;
  FocusNode _amountFocusNode;
  double _prevAmount;
  double _fee;
  int _selected = 1;

  @override
  void initState() {
    _amountController = TextEditingController(text: '0');
    _amountFocusNode = FocusNode();

    _countryBloc = CountryBloc();
    _loadCurrencyData();

    _amountController.addListener(_amountListener);
    super.initState();
  }

  _loadCurrencyData() {
    _currency = _countryBloc?.currency;
  }

  _amountListener() async {
    double _currentSenderAmount =
        double.tryParse(_amountController.text.replaceAll(' ', ''));

    if (_currentSenderAmount != _prevAmount) {
      await Future.delayed(Duration(milliseconds: 100), () {
        timer?.cancel();
      });

      await Future.delayed(Duration(milliseconds: 300), () {
        timer = Timer(Duration(seconds: 1), () async {
          if ((_amountController.text?.isNotEmpty ?? false) &&
              _currentSenderAmount != 0) {
            _prevAmount = _currentSenderAmount;
            if (mounted) {
              setState(() {
                _fee = HelperFunctions.calculateFee(_currentSenderAmount);
                _selected = (_fee == 0.0) ? 0 : 1;
              });
            }
          } else {
            if (mounted) {
              setState(() {
                _fee = null;
                _selected = 1;
              });
            }
          }
        });
      });
    }
  }

  @override
  void dispose() {
    _amountController.dispose();
    _amountFocusNode.dispose();
    super.dispose();
  }

  _next() {
    Navigator.of(context).pushNamed(Screens.SMS_VERIFICATION);
  }

  _onSelectDiffCurrency() {
    Navigator.of(context).pushNamed(Screens.COMMISSION_CALCULATION);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    bool _noSpacing = MediaQuery.of(context).viewInsets.bottom == 0.0;
    final List<Widget> feeLabels = [
      _getNoFeeText(),
      _getFeeText(),
    ];

    return Scaffold(
        appBar: CustomAppBar(
          title: 'Amount',
        ),
        extendBodyBehindAppBar: true,
        body: AppBackground(
          child: SafeArea(
            child: ScreenScrollViewWidget(
              shouldUseIntrinsicHeight: true,
              child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ScreenTitleInstructionWidget(
                      title: 'How much to Send?',
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: _noSpacing ? size.height * 0.15 : 0.05,
                        ),
                        AmountField(
                          amountController: _amountController,
                          amountFocusNode: _amountFocusNode,
                          currency: _currency,
                        ),
                        Padding(
                            padding: GenericAssets.paddingTop25,
                            child: AnimatedSwitcher(
                              duration: const Duration(milliseconds: 300),
                              child: feeLabels[_selected ?? 0],
                            ))
                      ],
                    ),
                    Spacer(),
                    Padding(
                      padding: GenericAssets.paddingBottom20,
                      child: GestureDetector(
                        onTap: _onSelectDiffCurrency,
                        child: Text(
                          'Send/Receive in different currencies',
                          style: GenericAssets.smallTitleDefault,
                        ),
                      ),
                    ),
                    Padding(
                        padding: GenericAssets.paddingTop10,
                        child: FullWidthButton('Continue', _next)),
                  ]),
            ),
          ),
        ));
  }

  Widget _getNoFeeText() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Icon(Icons.check, color: ColorHelper.genericSuccess),
        Text('\tNo Fee', style: GenericAssets.defaultTextLightGreen)
      ],
    );
  }

  Widget _getFeeText() {
    return _fee != null
        ? CrossFadeText<String>(
            initialData: '',
            data:
                '+ ${_fee.toStringAsFixed(2)} ${_currency.currencyName ?? 'CZK'} fee',
            builder: (value) => Text(value),
          )
        : Container();
  }
}
