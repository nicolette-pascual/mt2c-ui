import 'package:flutter/material.dart';
import 'package:mt2c_ui/models/page_view_model.dart';
import 'package:mt2c_ui/screens/home.dart';
import 'package:mt2c_ui/utils/constants/assets.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/widgets/overview.dart';

class InstructionsOverviewScreen extends StatefulWidget {
  static const routeName = Screens.WELCOME;
  @override
  _InstructionsOverviewScreenState createState() =>
      _InstructionsOverviewScreenState();
}

class _InstructionsOverviewScreenState
    extends State<InstructionsOverviewScreen> {
  _moveToHomePage() {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Home.routeName, (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Overview(
      pages: [
        PageViewModel(
            title: "1. Create an order online",
            body:
                "Choose where and how much you are planing to send. Fill in recepient details. Receive a 9-digit transfer number.",
            image: Image.asset(Assets.ONLINE_ICON, width: 150, height: 150)),
        PageViewModel(
            title: "2. Visit ATM",
            body:
                "Bring the required amount in cash.\nType the 9-digit transfer number.\nDeposit the cash.",
            image: Image.asset(Assets.ATM_ICON, width: 150, height: 150)),
        PageViewModel(
            title: "3. Done",
            body:
                "We will send a confirmation via SMS. And your recepient will get your transfer. \nNo wating in line.",
            image: Image.asset(Assets.CASH_ICON, width: 150, height: 150)),
      ],
      onDone: _moveToHomePage,
      onSkip: _moveToHomePage,
      onFinalSwipeCallback: _moveToHomePage,
    ));
  }
}
