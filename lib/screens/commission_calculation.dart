import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mt2c_ui/manager/country_manager.dart';
import 'package:mt2c_ui/models/country.dart';
import 'package:mt2c_ui/models/currency.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/designs/shader_mask_widget.dart';
import 'package:mt2c_ui/widgets/form_fields/currency_dropdown.dart';
import 'package:mt2c_ui/widgets/form_fields/themed_input_field.dart';
import 'package:mt2c_ui/widgets/full_width_button.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';
import 'package:mt2c_ui/widgets/screen_title_instruction.dart';

class CommissionCalculationScreen extends StatefulWidget {
  static const routeName = Screens.COMMISSION_CALCULATION;
  @override
  _CommissionCalculationScreenState createState() =>
      _CommissionCalculationScreenState();
}

class _CommissionCalculationScreenState
    extends State<CommissionCalculationScreen>
    with SingleTickerProviderStateMixin {
  Timer timer;
  List<Currency> _currenciesList;
  Currency _senderCurrency;
  Currency _receiverCurrency = ProjectConstants.currencies.first;

  Country _selectedCountry;

  AnimationController _animationController;
  Animation<Color> animationOne;
  Animation<Color> animationTwo;
  Color color1 = ColorHelper.white;
  Color color2 = ColorHelper.backgroundColor;

  TextEditingController _senderAmountController;
  TextEditingController _receiverAmountController;

  FocusNode _senderAmountFocusNode;
  FocusNode _receiverAmountFocusNode;

  bool _isLoading = true;

  double _prevSenderAmount;
  double _prevReceiverAmount;

  CountryBloc _countryBloc;

  @override
  void initState() {
    _senderAmountController = TextEditingController();
    _receiverAmountController = TextEditingController();
    _senderAmountFocusNode = FocusNode();
    _receiverAmountFocusNode = FocusNode();

    _senderAmountController.addListener(_amountListener);
    _receiverAmountController.addListener(_amountListener);

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    animationOne =
        ColorTween(begin: color1, end: color2).animate(_animationController);
    animationTwo =
        ColorTween(begin: color2, end: color1).animate(_animationController);

    _animationController.forward();

    _animationController.addListener(_animationListener);

    _countryBloc = CountryBloc();

    _loadCountryData();
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _senderAmountController.dispose();
    _receiverAmountController.dispose();
    _senderAmountFocusNode.dispose();
    _receiverAmountFocusNode.dispose();
    super.dispose();
  }

  _loadCountryData() {
    _selectedCountry = _countryBloc.selectedCountry;
    _senderCurrency = _countryBloc.currency;
    _currenciesList = _countryBloc.currencyList;
  }

  _animationListener() {
    if (_animationController.status == AnimationStatus.completed) {
      _animationController.reverse();
    } else if (_animationController.status == AnimationStatus.dismissed) {
      _animationController.forward();
    }
    this.setState(() {});
  }

  void _onChangeSenderCurrency(Currency value) {
    setState(() {
      _senderCurrency = value;
    });
  }

  void _onChangeReceiverCurrency(Currency value) {
    setState(() {
      _receiverCurrency = value;
    });
  }

  _amountListener() async {
    double _currentSenderAmount = double.tryParse(_senderAmountController.text);
    double _currentReceiverAmount =
        double.tryParse(_receiverAmountController.text);

    if ((_currentSenderAmount != _prevSenderAmount) ||
        _currentReceiverAmount != _prevReceiverAmount) {
      await Future.delayed(Duration(milliseconds: 100), () {
        timer?.cancel();
      });

      await Future.delayed(Duration(milliseconds: 300), () {
        timer = Timer(Duration(seconds: 1), () async {
          if ((_senderAmountController.text?.isNotEmpty ?? false) &&
              (_receiverAmountController.text?.isNotEmpty ?? false)) {
            _prevSenderAmount = _currentSenderAmount;
            _prevReceiverAmount = _currentReceiverAmount;
            setState(() {
              _animationController.stop();
              _isLoading = false;
            });
          }
        });
      });
    }
  }

  _next() {
    Navigator.of(context).pushNamed(Screens.SMS_VERIFICATION);
  }

  Widget _getShaderMaskLoadingIndicator() {
    return ShaderMaskWidget(
      colors: [animationOne.value, animationTwo.value],
      child: Container(
        width: 50,
        height: 15,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.0)),
          color: Colors.white,
        ),
      ),
    );
  }

  bool _isSameCurrency() {
    return _receiverCurrency == _senderCurrency;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: 'Amount',
      ),
      body: ScreenScrollViewWidget(
        shouldUseIntrinsicHeight: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ScreenTitleInstructionWidget(
              title: 'Send Cash ${'to ' + _selectedCountry.name ?? ''}',
            ),
            Expanded(
              child: Padding(
                padding: GenericAssets.paddingTop20,
                child: Column(
                  children: <Widget>[
                    _getFormFields(
                        'You send',
                        _currenciesList,
                        _senderCurrency,
                        _onChangeSenderCurrency,
                        _senderAmountController,
                        _senderAmountFocusNode,
                        nextFocusField: _receiverAmountFocusNode),
                    IntrinsicHeight(
                      child: Row(
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: 10),
                                  !(_isSameCurrency())
                                      ? Text('Rate:',
                                          style: GenericAssets
                                              .defaultTextSmallBold)
                                      : Container(),
                                  !(_isSameCurrency())
                                      ? Text('Fee:',
                                          style: GenericAssets
                                              .defaultTextSmallBold)
                                      : Container(),
                                ],
                              ),
                              SizedBox(width: 5),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: 10),
                                  _isLoading
                                      ? _getShaderMaskLoadingIndicator()
                                      : !(_isSameCurrency())
                                          ? Text(
                                              '1 ${_receiverCurrency.currencyName} = 2.99 ${_senderCurrency.currencyName}',
                                              style: GenericAssets
                                                  .defaultTextSmallOrangeBold)
                                          : Container(),
                                  SizedBox(width: 5),
                                  _isLoading
                                      ? _getShaderMaskLoadingIndicator()
                                      : !(_isSameCurrency())
                                          ? Text(
                                              '200 ${_receiverCurrency.currencyName}',
                                              style: GenericAssets
                                                  .defaultTextSmallOrangeBold)
                                          : Container()
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: GenericAssets.paddingTop15,
                      child: _getFormFields(
                          'Recepient gets',
                          _currenciesList,
                          _receiverCurrency,
                          _onChangeReceiverCurrency,
                          _receiverAmountController,
                          _receiverAmountFocusNode),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: GenericAssets.paddingTop10,
              child: FullWidthButton('Confirm', _next),
            )
          ],
        ),
      ),
    );
  }

  Widget _getFormFields(fieldName, dropdownValues, selectedDropdownVal,
      onChanged, controller, focusNode,
      {FocusNode nextFocusField}) {
    return Container(
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Flexible(
            child: ThemedInputField(
              fieldName: fieldName,
              controller: controller,
              focusNode: focusNode,
              textInputAction: (nextFocusField == null)
                  ? TextInputAction.done
                  : TextInputAction.next,
              nextFocusField: nextFocusField,
              keyboardType: TextInputType.number,
              textStyle: GenericAssets.inputTextLargeDarkGrey,
            ),
          ),
          CurrencyDropdown(
            values: dropdownValues,
            selectedValue: selectedDropdownVal,
            onChanged: onChanged,
          )
        ],
      ),
    );
  }
}
