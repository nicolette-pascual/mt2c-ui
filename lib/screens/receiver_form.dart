import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/form_divider.dart';
import 'package:mt2c_ui/widgets/form_fields/themed_input_field.dart';
import 'package:mt2c_ui/widgets/full_width_button.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';
import 'package:mt2c_ui/widgets/screen_title_instruction.dart';

class ReceiverFormScreen extends StatefulWidget {
  static const routeName = Screens.NEW_RECEIVER_FORM;
  @override
  _ReceiverFormScreenState createState() => _ReceiverFormScreenState();
}

class _ReceiverFormScreenState extends State<ReceiverFormScreen> {
  FocusNode _firstNameFocusNode,
      _surnameFocusNode,
      _middleNameFocusNode,
      _phoneNumberFocusNode;

  @override
  void initState() {
    _firstNameFocusNode = FocusNode();
    _surnameFocusNode = FocusNode();
    _middleNameFocusNode = FocusNode();
    _phoneNumberFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _firstNameFocusNode.dispose();
    _surnameFocusNode.dispose();
    _middleNameFocusNode.dispose();
    _phoneNumberFocusNode.dispose();
    super.dispose();
  }

  _onSubmit() {
    Navigator.of(context).pushNamed(Screens.TRANSFER_SUMMARY);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: 'Receiver',
        ),
        body: ScreenScrollViewWidget(
            shouldUseIntrinsicHeight: true,
            child: Column(
              children: <Widget>[
                ScreenTitleInstructionWidget(
                  title: "To Whom Are You Sending?",
                  instruction: "Provide details of the receiver",
                ),
                Expanded(child: _buildReceiverNameForm()),
                FullWidthButton('Continue', _onSubmit,
                    customPadding: GenericAssets.paddingSymV10)
              ],
            )));
  }

  Widget _buildReceiverNameForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: GenericAssets.paddingTop25,
          child: ThemedInputField(
            useDefaultContentPadding: true,
            padding: GenericAssets.paddingSymV5,
            fieldName: 'Name',
            textInputAction: TextInputAction.next,
            focusNode: _firstNameFocusNode,
            nextFocusField: _surnameFocusNode,
          ),
        ),
        FormDivider(),
        ThemedInputField(
          padding: GenericAssets.paddingSymV5,
          useDefaultContentPadding: true,
          fieldName: 'Surname',
          textInputAction: TextInputAction.next,
          focusNode: _surnameFocusNode,
          nextFocusField: _middleNameFocusNode,
        ),
        FormDivider(),
        ThemedInputField(
          padding: GenericAssets.paddingSymV5,
          useDefaultContentPadding: true,
          fieldName: 'Middle Name (optional)',
          textInputAction: TextInputAction.next,
          focusNode: _middleNameFocusNode,
          nextFocusField: _phoneNumberFocusNode,
        ),
        Padding(
          padding: GenericAssets.paddingTop15,
          child: ThemedInputField(
            padding: GenericAssets.paddingSymV5,
            useDefaultContentPadding: true,
            fieldName: 'Phone Number',
            prefixText: '+',
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.phone,
            focusNode: _phoneNumberFocusNode,
          ),
        ),
      ],
    );
  }
}
