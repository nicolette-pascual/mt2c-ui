import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/assets.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/atm_instructions_widget.dart';
import 'package:mt2c_ui/widgets/designs/arc_widget.dart';
import 'package:mt2c_ui/widgets/designs/dashed_line.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';

class TransferDetailsScreen extends StatefulWidget {
  static const routeName = Screens.TRANSFER_DETAILS;
  @override
  _TransferDetailsScreenState createState() => _TransferDetailsScreenState();
}

class _TransferDetailsScreenState extends State<TransferDetailsScreen>
    with TickerProviderStateMixin {
  AnimationController _pulseController;
  AnimationController _textStyleController;

  Animation<TextStyle> animationTextStyle;
  Animation<double> animationScale;

  bool _visible = false;

  @override
  void initState() {
    super.initState();
    _pulseController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _textStyleController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    animationTextStyle = TextStyleTween(
            begin: GenericAssets.xLargeOrange1, end: GenericAssets.xLargeBlue1)
        .animate(_textStyleController);
    animationScale =
        CurvedAnimation(parent: _pulseController, curve: Curves.bounceOut);

    _startTextStyleAnimation();
    _startTitleTextAnimation();
  }

  _startTextStyleAnimation() {
    _textStyleController.forward();
    _textStyleController.addListener(_textStyleAnimationListener);
  }

  _textStyleAnimationListener() {
    if (_textStyleController.status == AnimationStatus.completed) {
      _textStyleController.reverse();
    } else if (_textStyleController.status == AnimationStatus.dismissed) {
      _textStyleController.forward();
    }
    this.setState(() {});
  }

  _startTitleTextAnimation() async {
    _pulseController.forward();
    await Future.delayed(Duration(milliseconds: 500), () {
      setState(() {
        _visible = true;
      });
    });
  }

  @override
  void dispose() {
    _pulseController.dispose();
    _textStyleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.5;
    var size = MediaQuery.of(context).size;
    var applyWidth = MediaQuery.of(context).size.shortestSide > 600;

    return Scaffold(
        backgroundColor: Colors.white,
        body: ScreenScrollViewWidget(
          shouldUseIntrinsicHeight: true,
          customPadding: GenericAssets.paddingNone,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Flexible(
                  flex: 2,
                  child: Container(child: _getStackHeader(size, applyWidth))),
              Flexible(
                flex: 1,
                child: AtmInstructionsWidget(
                  isSuccessPage: true,
                ),
              ),
            ],
          ),
        ));
  }

  Widget _getStackHeader(size, applyWidth) {
    return Stack(
      children: <Widget>[
        Positioned(
            top: 40,
            left: 20,
            right: 20,
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7fcecece),
                          offset: Offset(5, 10),
                          blurRadius: 16,
                          spreadRadius: 7)
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(height: size.height * 0.15),
                      Padding(
                        padding: GenericAssets.paddingTop25,
                        child: AnimatedOpacity(
                          duration: Duration(milliseconds: 300),
                          opacity: _visible ? 1 : 0,
                          child: Text('Transfer Created',
                              style: GenericAssets.titleDefault),
                        ),
                      ),
                      Padding(
                        padding: GenericAssets.padding20,
                        child: AnimatedOpacity(
                          duration: Duration(milliseconds: 300),
                          opacity: _visible ? 1 : 0,
                          child: Text(
                            "Please deposit money in next 24 hours in any ATMs",
                            style: GenericAssets.smallTitleGray,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      SizedBox(width: size.width * 0.75, child: DashedLine()),
                      Padding(
                        padding: GenericAssets.padding20,
                        child: AnimatedDefaultTextStyle(
                          duration: Duration(milliseconds: 300),
                          style: animationTextStyle.value,
                          child: Text("392  232  232  282",
                              //style: GenericAssets.xLargeBlue1,
                              textAlign: TextAlign.center),
                        ),
                      ),
                      Text("Transfer number",
                          style: GenericAssets.smallTitleGray),
                      Image.asset(Assets.LOGO_NAME,
                          width: size.width * 0.2, height: size.height * 0.1)
                    ],
                  ),
                ),
                Image.asset(
                  Assets.PAPER_BG,
                  fit: BoxFit.fitWidth,
                  width: applyWidth
                      ? MediaQuery.of(context).size.width * 0.75
                      : MediaQuery.of(context).size.width,
                )
              ],
            )),
        Positioned.fill(
          child: ArcWidget(
            arcPointA: 0.140,
            arcPointB: 0.30,
            arcPointC: 0.140,
          ),
        ),
        Positioned(
            top: size.height * 0.1,
            left: 0,
            right: 0,
            child: Hero(
                tag: 'create',
                flightShuttleBuilder: (
                  BuildContext flightContext,
                  Animation<double> animation,
                  HeroFlightDirection flightDirection,
                  BuildContext fromHeroContext,
                  BuildContext toHeroContext,
                ) {
                  return AnimatedBuilder(
                    animation: animation,
                    builder: (context, value) {
                      return _checkIcon();
                    },
                  );
                },
                child: ScaleTransition(
                  scale: animationScale,
                  child: _checkIcon(),
                ))),
      ],
    );
  }

  Widget _checkIcon() {
    return Container(
      width: 72.0,
      height: 72.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: ColorHelper.primaryColor,
      ),
      child: Icon(Icons.check, color: Colors.white, size: 40),
    );
  }
}
