import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/country_flag_with_name.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/designs/arc_widget.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';

class TransferSummaryScreen extends StatefulWidget {
  static const routeName = Screens.TRANSFER_SUMMARY;
  @override
  _TransferSummaryScreenState createState() => _TransferSummaryScreenState();
}

class _TransferSummaryScreenState extends State<TransferSummaryScreen> {
  _onNext() {
    Navigator.of(context).pushNamed(Screens.TRANSFER_DETAILS);
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.5;
    var size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: CustomAppBar(
          title: 'Confirmation',
        ),
        backgroundColor: ColorHelper.white,
        extendBodyBehindAppBar: true,
        body: ArcWidget(
          useAsBackground: true,
          arcPointA: 0.85,
          arcPointB: 0.95,
          arcPointC: 0.85,
          child: ScreenScrollViewWidget(
            shouldUseIntrinsicHeight: true,
            customPadding: GenericAssets.paddingNone,
            child: _getReceiverColumn(size),
          ),
        ));
  }

  Widget _getReceiverColumn(size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: size.height * 0.15),
        _getInfoFields('Receiver:', _getReceiverInfo(), rowTitle: 'To:'),
        _getInfoFields('ATM to Visit:', _getBranchInfo(),
            rowTitle: 'Address:', spacer: SizedBox(width: size.width * 0.1)),
        _getInfoFields('Amount and Fee', _getAmountAndFee(size),
            showDivider: false),
        Spacer(flex: 8),
        Hero(
          tag: 'create',
          child: RawMaterialButton(
            onPressed: _onNext,
            elevation: 2.0,
            fillColor: ColorHelper.primaryColor,
            child: Text('Create', style: GenericAssets.buttonText),
            padding: EdgeInsets.all(45.0),
            shape: CircleBorder(),
          ),
        ),
        Spacer()
      ],
    );
  }

  Widget _getReceiverInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CountryWidget(
            country: ProjectConstants.countries.first,
            style: GenericAssets.defaultText,
            flagHeight: 20,
            flagWidth: 30),
        Text('Katerina Ivanchenko'),
        Text('+3 953 020 232', style: GenericAssets.defaultTitleGray)
      ],
    );
  }

  Widget _getBranchInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            Icon(Icons.location_on),
            Text('INFOBUS'),
          ],
        ),
        Text("Na Florenci 1332,\n110 00 Praha-Nové Město",
            style: GenericAssets.defaultTitleGray),
      ],
    );
  }

  Widget _getAmountAndFee(size) {
    return Row(
      children: <Widget>[
        SizedBox(width: size.width * 0.1),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Amount:', style: GenericAssets.smallTitleBlue),
            Text('Fee:', style: GenericAssets.smallTitleBlue),
            Padding(
                padding: GenericAssets.paddingTop25,
                child: Text('Total to Pay:'))
          ],
        ),
        SizedBox(width: size.width * 0.05),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text("20 000 CZK"),
            Text("400 CZK"),
            Padding(
              padding: GenericAssets.paddingTop20,
              child:
                  Text("20 400 CZK", style: GenericAssets.defaultTextBlue4Bold),
            )
          ],
        )
      ],
    );
  }

  Widget _getInfoFields(String title, Widget info,
      {String rowTitle, bool showDivider = true, Widget spacer}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: GenericAssets.paddingSymV10H15,
          child: Text(title ?? '', style: GenericAssets.defaultTextBlueBold),
        ),
        Padding(
          padding: GenericAssets.paddingLeft20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  spacer ?? Container(),
                  info,
                ],
              ),
              showDivider
                  ? Padding(
                      padding: GenericAssets.paddingTop15,
                      child: Divider(endIndent: 15),
                    )
                  : Container()
            ],
          ),
        )
      ],
    );
  }
}
