import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mt2c_ui/utils/constants/color_helper.dart';
import 'package:mt2c_ui/utils/constants/project_constants.dart';
import 'package:mt2c_ui/utils/constants/screens.dart';
import 'package:mt2c_ui/utils/constants/ui_helper.dart';
import 'package:mt2c_ui/widgets/countdown_timer.dart';
import 'package:mt2c_ui/widgets/custom_appbar.dart';
import 'package:mt2c_ui/widgets/form_fields/themed_input_field.dart';
import 'package:mt2c_ui/widgets/full_width_button.dart';
import 'package:mt2c_ui/widgets/screen_scroll_view_widget.dart';
import 'package:mt2c_ui/widgets/screen_title_instruction.dart';

class SmsVerificationScreen extends StatefulWidget {
  static const routeName = Screens.SMS_VERIFICATION;
  @override
  _SmsVerificationScreenState createState() => _SmsVerificationScreenState();
}

class _SmsVerificationScreenState extends State<SmsVerificationScreen> {
  bool _verificationFormUsed = false;
  int selected;

  final rightAnimation = Tween<Offset>(
      begin: Offset(2.5, 0.0), end: Offset(0.0, 0.0)); // from right
  final leftAnimation = Tween<Offset>(
      begin: Offset(-2.5, 0.0), end: Offset(0.0, 0.0)); // from left

  final Curve curveAnimation = Curves.easeIn;

  @override
  void initState() {
    super.initState();
    selected = 0;
  }

  _requestSmsCode() {
    setState(() {
      _verificationFormUsed = true;
      selected = 1;
    });
  }

  _verifySmsCode() {
    Navigator.of(context).pushNamed(Screens.NEW_RECEIVER_FORM);
  }

  Future<bool> _onWillPop() async {
    if (_verificationFormUsed) {
      _updateFormUsed();
      return false;
    } else {
      return true;
    }
  }

  _updateFormUsed() {
    setState(() {
      _verificationFormUsed = false;
      selected = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> forms = [
      _buildSmsRequestCodeForm(),
      _buildSmsVerificationCodeForm(),
    ];

    return Scaffold(
      appBar: CustomAppBar(
        title: 'Phone Verification',
        onLeadingPressed: _verificationFormUsed ? _updateFormUsed : null,
      ),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: ScreenScrollViewWidget(
          shouldUseIntrinsicHeight: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: GenericAssets.paddingBottom24,
                child: ScreenTitleInstructionWidget(
                  title: 'Mobile Number',
                  instruction:
                      "Please fill in your phone number. We will send SMS code to verify it.",
                ),
              ),
              _buildAnimatedForm(forms),
              Spacer(),
              FullWidthButton(
                  _verificationFormUsed ? 'Verify Number' : 'Receive Code',
                  _verificationFormUsed ? _verifySmsCode : _requestSmsCode)
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAnimatedForm(forms) {
    return AnimatedSwitcher(
      switchInCurve: curveAnimation,
      switchOutCurve: curveAnimation,
      transitionBuilder: (Widget child, Animation<double> animation) {
        // Compares the child key with the current ValueKey
        // to determine what kind of in and out animation should be done for
        // specific forms
        if (child.key == ValueKey(selected)) {
          return SlideTransition(
            child: child,
            position: child.key == ValueKey(0) // Request Code Form
                ? leftAnimation.animate(animation) // in animation - from left
                : rightAnimation.animate(animation), // out animation - to right
          );
        } else {
          return SlideTransition(
            child: child,
            position: child.key == ValueKey(1) // Code Verification Form
                ? rightAnimation.animate(animation) // in animation - from right
                : leftAnimation.animate(animation), // out animation - to left
          );
        }
      },
      duration: Duration(milliseconds: 300),
      child: forms[selected],
    );
  }

  Widget _buildSmsRequestCodeForm() {
    return SmsRequestCodeForm(key: ValueKey<int>(0));
  }

  Widget _buildSmsVerificationCodeForm() {
    return SmsVerificationCodeForm(key: ValueKey<int>(1));
  }
}

class SmsRequestCodeForm extends StatefulWidget {
  const SmsRequestCodeForm({Key key}) : super(key: key);

  @override
  _SmsRequestCodeFormState createState() => _SmsRequestCodeFormState();
}

class _SmsRequestCodeFormState extends State<SmsRequestCodeForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ThemedInputField(
          fieldName: 'Phone Number',
          prefixText: '+',
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.phone,
        ),
        SizedBox(
          height: 115,
        )
      ],
    );
  }
}

class SmsVerificationCodeForm extends StatefulWidget {
  const SmsVerificationCodeForm({Key key}) : super(key: key);

  @override
  _SmsVerificationCodeFormState createState() =>
      _SmsVerificationCodeFormState();
}

class _SmsVerificationCodeFormState extends State<SmsVerificationCodeForm> {
  bool _hasTimerStopped = false;
  Timer _timer;
  int _counter = ProjectConstants.smsTimerDuration;

  @override
  void initState() {
    setCountdownTimer(ProjectConstants.smsTimerDuration);
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  setCountdownTimer(duration) {
    if (_counter == 0) {
      _counter += duration;
    }

    _timer = Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) => setState(
        () {
          if (_counter < 1) {
            timer.cancel();
            _hasTimerStopped = true;
          } else {
            _counter = _counter - 1;
          }
        },
      ),
    );
  }

  void onResendSms() {
    setState(() {
      setCountdownTimer(ProjectConstants.smsTimerDuration);
      _hasTimerStopped = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Padding(
                padding: GenericAssets.padding15,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 25, right: 10),
                      child: Icon(Icons.check_circle_outline,
                          color: ColorHelper.blue4),
                    ),
                    Expanded(
                      child: Text("Code sent to +420 915 622 727",
                          style: GenericAssets.defaultTextBlue4),
                    ),
                  ],
                ),
              ),
              Divider(
                indent: 15,
                endIndent: 15,
              ),
              ThemedInputField(
                fieldName: 'Code from SMS',
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
        _hasTimerStopped
            ? InkWell(
                onTap: onResendSms,
                child: Padding(
                  padding: GenericAssets.paddingTop25,
                  child: Text(
                    'Resend SMS',
                    style: GenericAssets.smallTitleDefault,
                  ),
                ),
              )
            : CountdownTimer(
                padding: GenericAssets.paddingTop25,
                text:
                    Text('$_counter sec', style: GenericAssets.smallTitleGray),
              )
      ],
    );
  }
}
